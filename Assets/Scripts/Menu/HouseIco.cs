﻿using UnityEngine;
using System.Collections;

public class HouseIco : MonoBehaviour {

	// variables
	float z;
	bool is_enabled = false;
	bool is_hero = true;

	// Use this for initialization
	void Start () {
		z = gameObject.transform.position.z;
		FacePicker.twinkle_changed += Twinkle;
		State.hero_changed += IcoChanged;

		IcoChanged ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Notifications
	public void IcoChanged(object unused = null) {
		if (State.Instance.hero != null && gameObject.name.EndsWith (" " + State.Instance.hero.house.ToString ())) {
			is_enabled = true;
			is_hero = true;
			return;
		} else if (gameObject.name.EndsWith (" " + State.Instance.GetBoss(Passport.Jason).house)) {
			if (State.Instance.is_kids_detector || State.Instance.is_hero_detector) {
				is_enabled = true;
				is_hero = false;
				return;
			}
		}

		gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, z);
		is_enabled = false;
	}

	public void Twinkle(bool is_visible) {
		if (!is_enabled)
			return;

		if (is_visible)
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
		else
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z + CameraControl.min_z * 2);
	}
}
