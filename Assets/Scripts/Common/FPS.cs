﻿/* Note: this script downloaded from wikipedia */

using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour
{
	private TextMesh mesh;
	private float deltaTime = 0.0f;
	private float min_fps = float.MaxValue;

	// Use this for initialization
	void Start() {
		mesh = GetComponent<TextMesh> ();
	}

	// Update is called once per frame
	void Update() {
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		//int w = Screen.width, h = Screen.height;
		//GUIStyle style = new GUIStyle();
		//Rect rect = new Rect(0, 0, w, h * 2 / 100);
		//style.alignment = TextAnchor.UpperLeft;
		//style.fontSize = h * 2 / 100;
		//style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);
		//float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		if (fps < min_fps)
			min_fps = fps;
		string text = string.Format("{0:0.}", fps);
		mesh.text = text;
	}
}
