﻿using UnityEngine;
using System.Collections;

public class ScreenEffect : MonoBehaviour {
	// variables
	private Material material;
	public Shader shader;

	// settings
	public bool is_blur { set { material.SetInt ("_is_blur", value ? 1 : 0); } }
	public bool is_fintensity { set { material.SetInt ("_is_fintensity", value ? 1 : 0); } }
	public bool is_reflected { set { material.SetInt ("_is_reflected", value ? 1 : 0); } }
	public bool is_monochrome { set { material.SetInt ("_is_monochrome", value ? 1 : 0); } }
	public bool is_infared { set { material.SetInt ("_is_infared", value ? 1 : 0); } }

	// Use this for initialization
	void Start () {
		material = new Material (shader);
		material.SetFloat ("_step_x", 1.0f / 256);
		material.SetFloat ("_step_y", 1.0f / 224);

		// no effects
		is_blur = false;
		is_fintensity = false;
		is_reflected = false;
		is_monochrome = false;
		is_infared = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Post processing effects
	void OnRenderImage (RenderTexture src, RenderTexture dst) {
		Graphics.Blit(src, dst, material);
	}
}
