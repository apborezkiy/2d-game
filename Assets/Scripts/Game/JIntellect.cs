﻿using UnityEngine;
using System.Collections;
using Types;

public class JIntellect : Intellect {
	public enum DefferedActions { No = 0, GetIn = 1, GetTortureKids = 2, GetTortureHero = 3, Wait = 4 }
	public enum Tasks { SaveHero, SaveKids }

	///////////////
	// VARIABLES //
	///////////////

	// common
	public DefferedActions next_action = DefferedActions.No;
	private bool is_walk = true;
	private bool is_cabin = false;
	private bool is_shoot = false;

	// actions
	private const int torment_time = 60;
	private Passport victim = Passport.No;

	// cabin battle control
	private float z_original;
	private float x_original;
	private float y_original;
	private const float appear_time_1 = 0.35f;
	private const float appear_time_2 = 1.0f;
	private const int x_deflection = 50;
	private const int y_deflection = 10;

	// debug
	public TextMesh _debug_mesh = null;
	protected override TextMesh debug_mesh { get { return _debug_mesh; } }
	protected override string GetDebugLine() { return State.Instance.GetBoss (Passport.Jason).house.ToString(); }

	///////////////////////
	// FUNCTIONS: ENGINE //
	///////////////////////

	// constructor
	public JIntellect() : base(Spawn.Jason) {}

	//
	// Use this for initialization
	//
	protected override void Start() {
		base.Start ();
		Move ();
		
		z_original = transform.position.z;
	}

	//
	// Update is called once per frame
	//
	protected override void Update() {
		base.Update ();

		// check walk boundaries
		if (is_cabin && is_walk) {
			if (transform.position.x < x_original - x_deflection && direction == false) {
				Move ();
				direction = true;
			} else if (transform.position.x > x_original + x_deflection && direction == true) {
				Move ();
				direction = false;
			}
		}
	}

	///////////////////////
	// FUNCTIONS: COMMON //
	///////////////////////

	//
	// Notification when weapon hits to the jason
	//
	public override bool Hit(int aux_damage, int damage) {
		Boss boss = State.Instance.GetBoss (Passport.Jason);
		lifes = boss.health;
		if (base.Hit(damage, damage) == true) {
			// update health
			boss.health = lifes;
			boss.wounds += 1;
			if (lifes <= 0)
				my_animation.Switch("is_walk", false);
			else {
				if (boss.IsSurrender() && State.Instance.is_battle) {
					State.Instance.is_battle = false;
					my_animation.Twinkle(die_time * 2);
					Scheduler.Instance.Add(new Schedule(die_time * 2, false, new Notifier(StopBattle, true)));
				}
			}
			return true;
		} else
			return false;
	}

	//
	// Notification called when level complited
	//
	public override void NotifyDie() {
		State.Instance.is_the_end = true;
		Application.LoadLevel ("Menu");
	}

	/////////////////////////////////
	// FUNCTIONS: INTELLECT COMMON //
	/////////////////////////////////

	//
	// Is current general action can be changed ?
	//
	protected override bool IsActionCanChanged (Actions breed) {
		// intension busy
		if (State.Instance.is_battle || next_action != DefferedActions.No)
			return false;

		// can not leave the house with current hero
		if (State.Instance.hero.house == State.Instance.GetBoss(Passport.Jason).house)
			return false;

		return true;
	}

	//
	// Is current local action can be changed ?
	//
	protected override bool IsSubActionCanChanged() {
		if (next_action != DefferedActions.No)
			return false;

		if (is_shoot == true)
			return false;

		return true;
	}

	//
	// Notification called before every thought can be generated
	//
	protected override void SensesChanged() {
		Boss s = State.Instance.GetBoss (Passport.Jason);
		// direct senses
		SetSense (Sences.Win, s.CalculateWin);
		SetSense (Sences.Failure, s.CalculateFailure);
		SetSense (Sences.Survival, s.CalculateSurvival);
		SetSense (Sences.Pain, s.CalculatePain);
		SetSense (Sences.Fatigue, s.CalculateFatigue);
		SetSense (Sences.Interest, s.CalculateInterest);
		// indirect senses
		SetSense (Sences.Revenge, s.CalculateRevenge);
		SetSense (Sences.Boredom, s.CalculateBoredom);
		SetSense (Sences.Rage, s.CalculateRage);
	}

	//
	// Notification called when boss has different ways to go
	//
	public bool Fork(Location location, int house) {
		Boss boss = State.Instance.GetBoss (Passport.Jason);
		if (next_action == DefferedActions.GetIn && (location == Location.SmallHouse || location == Location.BigHouse)) {
			// get in
			next_action = DefferedActions.No;
			GetIn (location, house);
		} else if ((next_action == DefferedActions.GetTortureHero || next_action == DefferedActions.GetTortureKids) && location == Location.SmallHouse) {
			victim = State.Instance.GetHeroByHouse(house);
			if (victim != Passport.No) {
				// get in for torture
				next_action = DefferedActions.Wait;
				GetIn (location, house);
				// designate on the map
				State.Instance.is_hero_detector = true;
				State.Instance.hero = State.Instance.hero;
				GameObject.Find("Task Time").GetComponent<TaskTimer>().Enable(torment_time, new Notifier(TaskFailed, Tasks.SaveHero));
				// create acceptance speach
				Talks.CreateAny(Hero.GetName(victim), ActiveText.messages[2]);
			}
		} else if (next_action == DefferedActions.No && (location != Location.SmallHouse && location != Location.BigHouse && location != Location.WoodsHouse)) {
			if (Cabins.Instance.random.Next() % 2 == 0) {
				// go to the pass
				boss.location = location;
				return true;
			}
		}

		// skip teleport
		return false;
	}

	//
	// Notification called current hero's task failed
	//
	public void TaskFailed(object arg) {
		Audio.PlaySong (Audio.Tracks.Surprise);
		
		Tasks task = (Tasks)arg;
		
		switch (task) {
		case Tasks.SaveHero:
			Hero victim = State.Instance.GetHero(Hero.GetName(State.Instance.GetHeroByHouse(State.Instance.GetBoss(Passport.Jason).house)));
			victim.health = 0;
			victim.house = 0;
			GetOut ();
			break;
		case Tasks.SaveKids:
			State.Instance.kids -= 1;
			GetOut ();
			break;
		default:
			break;
		}
		
		GeneralThinking ();
	}

	////////////////////////////////////
	// FUNCTIONS: INTELLECT BEHAVIOUR //
	////////////////////////////////////

	//
	// Notification called to update object behaviour when GENERAL action changed
	//
	protected override void ActionChangedGeneral (ActionsGeneral action) {
		Boss boss = State.Instance.GetBoss (Passport.Jason);
		switch (action) {
		case ActionsGeneral.GetIn:
			next_action = DefferedActions.GetIn;
			break;
		case ActionsGeneral.GetOut:
			if (boss.house != 0) {
				GetOut();
			}
			break;
		case ActionsGeneral.Torture:
			if (State.Instance.IsSeveralHeroes()) {
				next_action = Cabins.Instance.random.Next() % 2 == 0 ? DefferedActions.GetTortureHero : DefferedActions.GetTortureKids;
			} else {
				next_action = DefferedActions.GetTortureKids;
			}
			break;
		}
	}

	//
	// Notification called to update object behaviour when OUTER action is changed
	//
	protected override void ActionChangedOuter (ActionsOut action) {
	}

	//
	// Notification called to update object behaviour when INNER SMALL action is changed
	//
	protected override void ActionChangedInnerSmall (ActionsInSC action) {
	}
		
	//
	// Notification called to update object behaviour when INNER BIG action is changed
	//
	protected override void ActionChangedInnerBig (ActionsInBC action) {
	}

	//
	// Notification called to update object behaviour when BATTLE action is changed
	//
	protected override void ActionChangedBattle (ActionsBattle action) {
		switch (action) {
		case ActionsBattle.Beat: /* blow */
			Audio.PlaySong(Audio.Tracks.Blow);
			Stop();
			GetComponent<Animator>().SetTrigger("shoot");
			Scheduler.Instance.Add(new Schedule(0.5f, false, new Notifier(delegate(object unused) { is_shoot = true; Move (); })));
			break;
		case ActionsBattle.Maneuver: /* y position */
			break;
		default:
			break;
		}
	}

	///////////////////////////////
	// FUNCTIONS: ATOMIC ACTIONS //
	///////////////////////////////

	//
	// Movement control
	//
	private void Move() {
		is_walk = true;
		is_shoot = false;
		Rigidbody2D body = GetComponent<Rigidbody2D> ();
		GetComponent<MyAnimationController> ().Switch ("is_cabin", is_cabin);
		GetComponent<MyAnimationController> ().Switch ("is_walk", is_walk);
		float speed = is_cabin ? State.Instance.GetBoss (Passport.Jason).walk_speed_in : State.Instance.GetBoss (Passport.Jason).walk_speed;
		body.velocity = new Vector2 (direction ? speed : -speed, body.velocity.y);
	}

	private void Stop() {
		is_walk = false;
		Rigidbody2D body = GetComponent<Rigidbody2D> ();
		body.velocity = new Vector2 (0.0f, body.velocity.y);
		GetComponent<MyAnimationController> ().Switch ("is_cabin", is_cabin);
		GetComponent<MyAnimationController> ().Switch ("is_walk", is_walk);
	}

	//
	// Get in/out control
	//
	private void GetIn(Location location, int house) {
		Boss boss = State.Instance.GetBoss (Passport.Jason);
		boss.location = location;
		boss.house = house;
		Cabins.Instance.LocateRandom (location, out boss.cabin_layer, out boss.cabin_rotation);
		is_cabin = true;
		Stop ();

		string scene = "Scene ";
		if (location == Location.SmallHouse)
			scene += "Small ";
		else if (location == Location.BigHouse)
			scene += "Big ";
		else if (location == Location.WoodsHouse)
			scene += "Woods ";

		GameObject target = GameObject.Find(scene + boss.cabin_layer.ToString() + " " + boss.cabin_rotation.ToString());
		float x = target.transform.position.x;
		float y = target.transform.position.y;
		transform.position = new Vector3 (x, y, CameraControl.max_z);

		TraceCurrentAction ((int)cur_action_kind, cur_action_value);
	}

	private void GetOut() {
		Boss boss = State.Instance.GetBoss (Passport.Jason);
		GameObject house = GameObject.Find ("Cabin " + boss.house.ToString());
		boss.location = house.GetComponent<Teleport>().src_location;
		boss.house = 0;
		State.Instance.hero = State.Instance.hero;
		transform.position = new Vector3 (house.transform.position.x + 35, house.transform.position.y - 67, z_original);
		direction = Cabins.Instance.random.Next() % 2 == 0 ? true : false;
		next_action = DefferedActions.No;
		is_cabin = false;
		Move();
	}

	//
	// Battle control
	//
	public void BattleIn() {
		State.Instance.GetBoss (Passport.Jason).Reset ();
		State.Instance.GetBoss (Passport.Jason).Occasion (Boss.Occasions.Battle);
		Scheduler.Instance.Add (new Schedule (appear_time_1, false, new Notifier(DoBattleIn)));
		GameObject.Find ("Task Time").GetComponent<TaskTimer> ().Disable ();
		x_original = transform.position.x;
		y_original = transform.position.y;
	}

	private void DoBattleIn(object unused = null) {
		transform.position = new Vector3 (transform.position.x, transform.position.y, z_original);
		Audio.StopAllMusic ();
		Audio.PlaySong (Audio.Tracks.Surprise);
		Scheduler.Instance.Add (new Schedule (appear_time_2, false, new Notifier(DoDoBattleIn)));
	}

	private void DoDoBattleIn(object unused = null) {
		Audio.PlayMusic (Audio.Tracks.Battle);
		hero.GetComponent<MyCharacterController>().InitBattle ();
	}

	public void StopBattle(object is_hero_win) {
		GetOut ();
		GeneralThinking ();
		hero.GetComponent<MyCharacterController> ().StopBattle ();

		// new weapon
		if ((bool)is_hero_win)
			State.Instance.GetBoss(Passport.Jason).weapon++;
		else
			State.Instance.GetBoss(Passport.Jason).weapon = WeaponType.No;
	}
}