﻿using UnityEngine;
using System.Collections;
using Types;

public class Action : MonoBehaviour {

	// variables
	private static float original_local_y;
	private static float original_local_x;
	private static bool is_message = false;
	private static bool _typed = true;
	private static bool typed {
		get { return _typed; }
		set {
			_typed = value;
			if (!_typed)
				is_message = true;
		}
	}

	// Use this for initialization
	void Start () {
		GameObject selector = GameObject.Find ("Action Selector");
		original_local_y = selector.transform.localPosition.y;
		original_local_x = selector.transform.localPosition.x;
		UpdateSelector (false, 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// Perform selected action
	public static void Perform(int action) {
		int house = State.Instance.hero.house;
		int layer = State.Instance.hero.cabin_layer;
		int rotation = State.Instance.hero.cabin_rotation;
		bool action_performed = false;
		Spawn spawn = Spawn.Empty;
		Passport passport = Passport.No;
		switch (action) {
		case 0: /* change */
			passport = Cabins.Instance.GetHeroLocated(house, layer, rotation);
			if (passport != Passport.No) {
				action_performed = true;
				State.Instance.hero = State.Instance.GetHero(Hero.GetName(passport));
				State.Instance.hero.cabin_layer = layer;
				State.Instance.hero.cabin_rotation = rotation;
				Cabins.Instance.LocateAborigines(house);
				GameObject.Find("Weapon Ico").GetComponent<Animator>().SetInteger ("Weapon", (int) State.Instance.hero.weapon);
				GameObject.Find("Face Ico").GetComponent<Animator>().SetInteger ("Face", (int) State.Instance.hero.passport);
				GameObject obj = GameObject.Find("Hero");
				obj.GetComponent<MyCharacterController>().UpdateLook(null);
				obj.GetComponent<SpriteRenderer> ().sharedMaterial.SetInt ("_Offset", State.Instance.hero.pallet);
			}
			break;
		case 1: /* pass */
			passport = Cabins.Instance.GetHeroLocated(house, layer, rotation);
			if (passport != Passport.No) {
				action_performed = true;
				WeaponType w1 = State.Instance.GetHero(Hero.GetName(passport)).weapon;
				WeaponType w2 = State.Instance.hero.weapon;
				State.Instance.GetHero(Hero.GetName(passport)).weapon = w2;
				State.Instance.hero.weapon = w1;
			}
			break;
		case 2: /* cure */
			passport = Cabins.Instance.GetHeroLocated(house, layer, rotation);
			if (passport != Passport.No && State.Instance.hero.compotes > 0) {
				if (State.Instance.GetHero(Hero.GetName(passport)).health < Hero.max_health) {
					action_performed = true;
					State.Instance.hero.compotes--;
					State.Instance.GetHero(Hero.GetName(passport)).health += Hero.compote_curing;
				}
			}
			break;
		case 3: /* take */
			spawn = Cabins.Instance.Take(house, layer, rotation);
			if (spawn != Spawn.Empty) {
				action_performed = true;
				switch (spawn) {
				case Spawn.Stone:
					State.Instance.hero.weapon = WeaponType.Stone;
					break;
				case Spawn.Knife:
					State.Instance.hero.weapon = WeaponType.Knife;
					break;
				case Spawn.MacheteInHouse:
					State.Instance.hero.weapon = WeaponType.Machete;
					break;
				case Spawn.AxeInHouse:
					State.Instance.hero.weapon = WeaponType.Axe;
					break;
				case Spawn.FireInHouse:
					State.Instance.hero.weapon = WeaponType.Torch;
					break;
				case Spawn.Letter:
					GameObject.Find("Hero").GetComponent<MyCharacterController>().m_is_enabled = false;
					GameObject letter = GameObject.Find ("Letter Text");
					letter.GetComponent<ActiveText>().LoadText();
					letter.GetComponent<ActiveText>().Type(new Notifier(NotifyTyped));
					typed = false;
					break;
				}
			}
			break;
		case 4: /* use */
			spawn = Cabins.Instance.Use(house, layer, rotation, IsActionFeasibly);
			if (spawn != Spawn.Empty) {
				action_performed = true;
				switch (spawn) {
				case Spawn.FireInFireplace:
					Cabins.Instance.elements.Add(new Cabins.InterierElement(Spawn.FireInFireplace, house, rotation, layer));
					break;
				case Spawn.Door:
					Cabins.Instance.OpenDoor(house, layer, rotation);
					break;
				}

				// update interier
				GameObject.Find ("Hero").GetComponent<MyCharacterController> ().UpdateLook(null);
			}
			break;
		}

		if (action_performed)
			Audio.PlaySong (Audio.Tracks.Use);
	}

	// Notifications
	public static void NotifyTyped(object unused) {
		typed = true;
	}

	// Continue complex action
	public static void Continue() {
		if (typed) {
			GameObject.Find ("Hero").GetComponent<MyCharacterController> ().m_is_enabled = true;
			GameObject.Find ("Letter Text").GetComponent<TextMesh>().text = "";
			is_message = false;
		}
	}

	// Update selector position
	public static void UpdateSelector(bool visible, int position) {
		GameObject selector = GameObject.Find ("Action Selector");
		float previous_y = selector.transform.localPosition.y;
		float target_y = original_local_y - 15 * position;
		selector.transform.localPosition = new Vector3 (visible ? original_local_x : original_local_x + 255, target_y, selector.transform.localPosition.z);
		if (visible && previous_y != target_y)
			Audio.PlaySong (Audio.Tracks.Pick);
	}

	// Type warning
	public static void TypeWarning(int text_id) {
		GameObject.Find ("Hero").GetComponent<MyCharacterController> ().m_is_enabled = false;
		GameObject letter = GameObject.Find ("Letter Text");
		letter.GetComponent<TextMesh> ().text = ActiveText.messages [text_id];
		letter.GetComponent<ActiveText> ().LoadText ();
		letter.GetComponent<ActiveText> ().Type (new Notifier (NotifyTyped));
		typed = false;
	}

	// Is there some text is typing
	public static bool IsTyping() {
		return is_message;
	}

	// Filters
	public static bool IsActionFeasibly(Spawn spawn) {
		switch (spawn) {
		case Spawn.FireInFireplace:
			if (State.Instance.hero.lighters > 0) {
				State.Instance.hero.lighters--;
				State.Instance.hero.lit++;
				return true;
			}
			break;
		case Spawn.Door:
			if (State.Instance.hero.keys > 0) {
				State.Instance.hero.keys--;
				return true;
			}
			break;
		}

		return false;
	}
}
