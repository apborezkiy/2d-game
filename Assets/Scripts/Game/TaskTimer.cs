﻿using UnityEngine;
using System.Collections;
using Types;

public class TaskTimer : MonoBehaviour {
	private TextMesh _mesh;
	private int? text {
		set { _mesh.text = (value == null) ? "" : System.String.Format("{0:000}", value); }
	}

	private Notifier notifier;
	private int time = 0;
	private uint timer_id = 0;

	// settings
	public bool is_paused = false;

	// Use this for initialization
	void Start () {
		_mesh = GetComponent<TextMesh> ();
		Disable ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Control
	public void Enable(int seconds, Notifier callback) {
		time = seconds;
		notifier = callback;
		timer_id = Scheduler.Instance.Add (new Schedule (1.0f, true, new Notifier (TimeTick)));

		if (!is_paused)
			text = time;
	}

	public void Disable() {
		text = null;
		Scheduler.Instance.Del (ref timer_id);
	}

	// Notifications
	public void TimeTick(object unused = null) {
		if (is_paused)
			return;

		time -= 1;
		text = time;

		if (time == 0)
			notifier.Start ();
		else if (time < 0)
			Disable ();
	}
}
