﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Types;

public class Respawn : MonoBehaviour {
	// type defentions
	public class LocationGroup {
		// location description
		public List<Location> locations;
		public List<uint> max_count; /* maximal count on the scene togeather in the specified location list */
		public List<float> probability; /* probability of the appearance in the specified location list */
		
		// constructor
		public LocationGroup(Location [] locations, uint [] max_count, float [] probability) {
			this.locations = new List<Location>(locations);
			this.max_count = new List<uint>(max_count);
			this.probability = new List<float>(probability);
		}
	};

	public class WeatherGroup {
		// wheather description
		public List<Weather> weathers;
		public List<uint> max_count; /* maximal count on the scene togeather in the specified weather list */
		public List<float> probability; /* probability of the appearance in the specified weather list */
		
		// constructor
		public WeatherGroup(Weather [] weathers, uint [] max_count, float [] probability) {
			this.weathers = new List<Weather>(weathers);
			this.max_count = new List<uint>(max_count);
			this.probability = new List<float>(probability);
		}
	};

	public class Filter {
		// type defenitions
		public delegate bool Comparator(uint value, Filter filter);

		// parametres
		private Comparator comparator = null;
		private uint value = 0;
		private uint kills = 0;
		private uint jumps = 0;
		private uint battles = 0;

		// comparators
		public static bool Kills (uint value, Filter _this) { return State.Instance.hero.kills % value == 0 && State.Instance.hero.kills > _this.kills; }
		public static bool Jumps (uint value, Filter _this) { return State.Instance.hero.jumps % value == 0 && State.Instance.hero.jumps > _this.jumps; }
		public static bool Battles (uint value, Filter _this) { return State.Instance.hero.battles % value == 0 && State.Instance.hero.battles > _this.battles; }

		// constructor
		public Filter(Comparator comparator, uint value) {
			this.comparator = comparator;
			this.value = value;
		}

		// run filter
		public bool IsAllowed() { return comparator (value, this); }

		// remember statistics
		public void Remember() {
			kills = State.Instance.hero.kills;
			jumps = State.Instance.hero.jumps;
			battles = State.Instance.hero.battles;
		}
	};

	public class SpawnInfo {
		// spawn information
		public Spawn type; /* spawn object kind */
		public uint count; /* current count on the scene */
		public float height; /* offset y position relatively ground */
		public float reductor; /* reduce spawn probability with every taken item of same kind */

		// conditions
		public List<LocationGroup> location_groups; /* location equivalence class */
		public List<WeatherGroup> weather_groups; /* wheather equivalence class */
		public List<Filter> filters; /* guard conditions */

		// consturctor
		public SpawnInfo(Spawn type, float reductor, float height, float interval, bool is_loop) {
			this.type = type;
			this.height = height;
			this.reductor = reductor;
			this.count = 0;
			this.location_groups = new List<LocationGroup>();
			this.weather_groups = new List<WeatherGroup>();
			this.filters = new List<Filter>();
			Scheduler.Instance.Add(new Schedule(interval, is_loop, new Notifier(TrySpawn, type)));
		}

		// is all conditions for appearing in the current location are met
		public float GetProbability(Location location, Weather weather, uint count, float reductor) {
			// test filters
			foreach (Filter filter in filters) {
				if (filter.IsAllowed() == false)
					return 0.0f;
			}
			bool is_probably_1 = false;
			bool is_probably_2 = false;
			float probability = 1.0f;
			// target location search
			foreach (LocationGroup group in location_groups) {
				if (group.locations.Exists(_location => _location == location)) {
					int index = group.locations.FindIndex(_location => _location == location);
					// combine with location probability
					if (count < group.max_count[index]) {
						is_probably_1 = true;
						probability = group.probability[index];
						break;
					}
				}
			}
			// target weather search
			foreach (WeatherGroup group in weather_groups) {
				if (group.weathers.Exists(_weather => _weather == weather)) {
					int index = group.weathers.FindIndex(_weather => _weather == weather);
					// combine with weather probability
					if (count < group.max_count[index]) {
						is_probably_2 = true;
						probability *= group.probability[index];
						break;
					}
				}
			}
			// no suitable location found
			return (is_probably_1 == true && is_probably_2 == true)? probability * reductor: 0.0f;
		}
	};

	// variables
	private List<SpawnInfo> spawns = new List<SpawnInfo> ();
	private const float dispersion = 224 / 2 - 32;
	private const float center = 32;

	// prefabs
	private ArrayList prefabs = null;

	// randomizer
	System.Random random = new System.Random();

	// Use this for initialization
	void Start () {
		// init static prefabs
		if (prefabs == null) {
			prefabs = new ArrayList();
			prefabs.Add(Resources.Load("Prefabs/Zombie", typeof(GameObject))); /* 0 */
			prefabs.Add(Resources.Load("Prefabs/Lighter", typeof(GameObject))); /* 1 */
			prefabs.Add(Resources.Load("Prefabs/Compote", typeof(GameObject))); /* 2 */
			prefabs.Add(Resources.Load("Prefabs/Key", typeof(GameObject))); /* 3 */
			prefabs.Add(Resources.Load("Prefabs/Stone", typeof(GameObject))); /* 4 */
			prefabs.Add(Resources.Load("Prefabs/Knife", typeof(GameObject))); /* 5 */
			prefabs.Add(Resources.Load("Prefabs/Machete", typeof(GameObject))); /* 6 */
			prefabs.Add(Resources.Load("Prefabs/Aqua Zombie", typeof(GameObject))); /* 7 */
			prefabs.Add(Resources.Load("Prefabs/Raven", typeof(GameObject))); /* 8 */
			prefabs.Add(Resources.Load("Prefabs/Fox", typeof(GameObject))); /* 9 */
			prefabs.Add(Resources.Load("Prefabs/Wolf", typeof(GameObject))); /* 10 */
			prefabs.Add(Resources.Load("Prefabs/Letter", typeof(GameObject))); /* 11 */
			prefabs.Add(Resources.Load("Prefabs/Machete in house", typeof(GameObject))); /* 12 */
			prefabs.Add(Resources.Load("Prefabs/Axe in house", typeof(GameObject))); /* 13 */
			prefabs.Add(Resources.Load("Prefabs/Fire in house", typeof(GameObject))); /* 14 */
			prefabs.Add(Resources.Load("Prefabs/Boy", typeof(GameObject))); /* 15 */
			prefabs.Add(Resources.Load("Prefabs/Girl", typeof(GameObject))); /* 16 */
			prefabs.Add(Resources.Load("Prefabs/Kid", typeof(GameObject))); /* 17 */
			prefabs.Add(Resources.Load("Prefabs/Fireplace Fire", typeof(GameObject))); /* 18 */
			prefabs.Add(Resources.Load("Prefabs/Grate", typeof(GameObject))); /* 19 */
			prefabs.Add(Resources.Load("Prefabs/Lantern", typeof(GameObject))); /* 20 */
			prefabs.Add(null); /* 21 - door (phony) */
			prefabs.Add(Resources.Load("Prefabs/Soda", typeof(GameObject))); /* 22 */
		}

		// init spawn
		SpawnInfo spawn = null;
		// spawn - earth zombie
		spawn = new SpawnInfo (Spawn.EarthZombie, 1.0f, 8.0f, 2.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 2, 3}, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawns.Add (spawn);
		// spawn - lighter
		spawn = new SpawnInfo (Spawn.Lighter, 0.7f, 50, 5.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			0.7f, 0.8f, 0.9f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 0.5f, 0.1f
		}));
		spawn.filters.Add (new Filter(Filter.Kills, 3));
		spawns.Add (spawn);
		// spawn - compote
		spawn = new SpawnInfo (Spawn.Compote, 0.5f, 50, 10.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			0.2f, 0.2f, 0.3f, 0.7f, 0.9f, 0.5f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 0.5f, 0.1f
		}));
		spawn.filters.Add (new Filter(Filter.Kills, 5));
		spawn.filters.Add (new Filter(Filter.Jumps, 10));
		spawns.Add (spawn);
		// spawn - key
		spawn = new SpawnInfo (Spawn.Key, 0.5f, 50, 15.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			0.2f, 0.3f, 0.2f, 0.3f, 0.5f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 0.5f, 0.1f
		}));
		spawn.filters.Add (new Filter(Filter.Kills, 10));
		spawn.filters.Add (new Filter(Filter.Jumps, 5));
		spawns.Add (spawn);
		// spawn - weapon stone
		spawn = new SpawnInfo (Spawn.Stone, 0.5f, 50, 1.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			0.3f, 0.3f, 0.2f, 0.3f, 0.5f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 0.5f, 0.1f
		}));
		spawn.filters.Add (new Filter(Filter.Kills, 1));
		spawn.filters.Add (new Filter(Filter.Jumps, 1));
		spawns.Add (spawn);
		// spawn - weapon knife
		spawn = new SpawnInfo (Spawn.Knife, 0.5f, 50, 1.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			1.0f, 0.3f, 0.2f, 0.3f, 0.5f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 0.5f, 0.1f
		}));
		spawn.filters.Add (new Filter(Filter.Kills, 1));
		spawn.filters.Add (new Filter(Filter.Jumps, 1));
		spawns.Add (spawn);
		// spawn - weapon machete
		spawn = new SpawnInfo (Spawn.Machete, 0.5f, 50, 1.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.AroundLake, Location.AroundCave, Location.AroundWoods, Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue}, new float[] {
			1.0f, 0.3f, 0.2f, 0.3f, 0.5f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 0.5f, 0.1f
		}));
		spawn.filters.Add (new Filter(Filter.Kills, 1));
		spawn.filters.Add (new Filter(Filter.Jumps, 1));
		spawns.Add (spawn);
		// spawn - aqua zombie
		spawn = new SpawnInfo (Spawn.AquaZombie, 1.0f, -3.0f, 2.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.Lake }, new uint[] {
			uint.MaxValue }, new float[] {
			1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 2, 3}, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawns.Add (spawn);
		// spawn - raven
		spawn = new SpawnInfo (Spawn.Raven, 1.0f, 97.0f, 2.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.Lake, Location.AroundLake, Location.AroundCave, Location.AroundWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue, uint.MaxValue }, new float[] {
			1.0f, 1.0f, 1.0f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 2, 3}, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawns.Add (spawn);
		// spawn - fox
		spawn = new SpawnInfo (Spawn.Fox, 1.0f, 2.0f, 2.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue }, new float[] {
			1.0f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawns.Add (spawn);
		// spawn - wolf
		spawn = new SpawnInfo (Spawn.Wolf, 1.0f, 2.0f, 2.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.Cave }, new uint[] {
			uint.MaxValue }, new float[] {
			1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 1, 1}, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawns.Add (spawn);
		// spawn - soda
		spawn = new SpawnInfo (Spawn.Soda, 1.0f, 50f, 2.0f, true);
		spawn.location_groups.Add(new LocationGroup(new Location[] {
			Location.Cave, Location.NorthernWoods, Location.SouthernWoods }, new uint[] {
			uint.MaxValue, uint.MaxValue, uint.MaxValue }, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawn.weather_groups.Add(new WeatherGroup(new Weather[] {
			Weather.Morning, Weather.Evening, Weather.Night}, new uint[] {
			1, 2, 3}, new float[] {
			1.0f, 1.0f, 1.0f
		}));
		spawns.Add (spawn);
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	// Try to spawn object on the current area of argument type
	public static void TrySpawn(object arg) {
		// class reference
		Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();

		// search target breed object info
		Spawn type = (Spawn)arg;
		foreach (SpawnInfo spawn in respawn.spawns) {
			if (spawn.type != type)
				continue;

			float reductor = Mathf.Pow(spawn.reductor, State.Instance.GetStatistics(spawn.type, spawn.count));
			float probability = spawn.GetProbability(State.Instance.hero.location, State.Instance.weather, spawn.count, reductor);
			if (IsProbably(probability, respawn.random) == true) {
				// register new spawn object
				spawn.count++;

				// remember statistics
				spawn.filters.ForEach(delegate(Filter filter) { filter.Remember(); });

				// hero position
				GameObject hero = GameObject.Find("Hero");
				float x = hero.transform.position.x;
				float y = hero.transform.position.y;

				// prefab
				GameObject prefab = (GameObject)respawn.prefabs[(int)type];
				float x_correction = prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x;

				// search nearest ground
				float distance_x = float.MaxValue;
				float dispersion_left = float.MaxValue;
				float dispersion_right = float.MaxValue;
				float offset_x = 0.0f;
				float pos_max = 0.0f;
				float pos_min = 0.0f;
				bool ground_found = false;
				GameObject [] grounds = GameObject.FindGameObjectsWithTag ("Ground");
				foreach (GameObject ground in grounds) {
					if (hero.transform.position.y < ground.transform.position.y)
						continue;
					ground_found = true;
					float delta_x = Mathf.Abs(hero.transform.position.x - ground.transform.position.x);
					float delta_y = Mathf.Abs(hero.transform.position.y - ground.transform.position.y);
					if (delta_y < 224 && delta_x < distance_x) {
						distance_x = delta_x;
						offset_x = 0.0f;
						Collider2D collider = ground.GetComponent<Collider2D>();
						y = collider.transform.position.y + collider.bounds.size.y / 2.0f;
						pos_max = collider.transform.position.x - x_correction + collider.bounds.size.x / 2.0f;
						pos_min = collider.transform.position.x + x_correction - collider.bounds.size.x / 2.0f;
						dispersion_right = pos_max - hero.transform.position.x;
						dispersion_left = hero.transform.position.x - pos_min;
						if (dispersion_right < 0)
						{
							offset_x = dispersion_right;
							dispersion_left = dispersion_left + dispersion_right;
							dispersion_right = 0;
						}
						if (dispersion_left < 0)
						{
							offset_x = -dispersion_left;
							dispersion_right = dispersion_left + dispersion_right;
							dispersion_left = 0;
						}
					}
				}
				// spawn
				if (ground_found == true) {
					MyCharacterController mcc = hero.GetComponent<MyCharacterController>();
					int interval = Math.Min((int)dispersion, (int)(mcc.PickStraight(Mathf.Abs(dispersion_left), Mathf.Abs(dispersion_right)) / 2.0));
					x += offset_x + mcc.CastStraight(center + respawn.random.Next() % (1 + interval));
					GameObject obj = (GameObject)Instantiate (prefab, new Vector3 (x, y + spawn.height, prefab.transform.position.z), Quaternion.identity);
					Enemy enemy = obj.GetComponent<Enemy>();
					if (enemy != null) {
						enemy.pos_max = pos_max;
						enemy.pos_min = pos_min;
					}
				}

				// spawn info is found
				break;
			}
		}
	}

	// Create unrecorded spawn at the specified position
	public void CreateSpawn (Spawn type, float x, float y) {
		Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();
		GameObject prefab = (GameObject)respawn.prefabs[(int)type];
		if (prefab != null)
			Instantiate (prefab, new Vector3 (x, y, prefab.transform.position.z), Quaternion.identity);
	}

	// Delete unrecorded spawn
	public void DeleteSpawn (Spawn type) {
		Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();
		GameObject prefab = (GameObject)respawn.prefabs[(int)type];
		if (prefab != null)
			Destroy(GameObject.Find (prefab.name + "(Clone)"));
	}

	// Subtract count of specified object kind
	public static void Delete(Spawn type) {
		// class reference
		Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();

		// search object of specified kind
		foreach (SpawnInfo spawn in respawn.spawns) {
			if (spawn.type == type) {
				spawn.count--;
				break;
			}
		}
	}

	// Probability simulation
	private static bool IsProbably(float probability, System.Random random) {
		for (int i = Mathf.CeilToInt(1.0f / probability) - 1; i != 0; --i) {
			// assert even numbers as eagle of coin
			if (random.Next() % 2 != 0)
				return false;
		}
		// eagle of coin in all attempts
		return true;
	}
}
