﻿using UnityEngine;
using System.Collections;

public class Perspective : MonoBehaviour {
	// settings
	private const float scale = 0.5f;

	// variables
	private float original_x;
	private float offset_x = 0.0f;

	// references
	private Transform target;

	// Use this for initialization
	void Start () {
		original_x = transform.position.x;
		target = GameObject.Find ("Main Camera").transform;
	}
	
	// Update is called once per frame
	void Update () {
		float offset_x = target.position.x * scale;
		if (offset_x != this.offset_x) {
			this.offset_x = offset_x;
			transform.position = new Vector3(original_x + offset_x, transform.position.y, transform.position.z);
		}
	}
}
