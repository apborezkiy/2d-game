﻿using UnityEngine;
using System.Collections;
using Types;

public class Weapon : MonoBehaviour {
	// settings
	public int enemy_damage;
	public int boss_damage;
	public float life_time = 0.0f;
	public float horizontal_x_component;
	public float horizontal_y_component;
	public float vertical_x_component;
	public float vertical_y_component;
	public float horizontal_x_offset = 0.0f;
	public float horizontal_y_offset = 0.0f;
	public float vertical_x_offset = 0.0f;
	public float vertical_y_offset = 0.0f;
	private const float range = 224.0f;
	private bool breaked = false;
	public bool is_cabin = false;
	private const float cabin_range = 50.0f;

	// references
	private Transform hero;

	// Use this for initialization
	void Start () {
		hero = GameObject.Find ("Hero").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (!is_cabin) {
			if (Mathf.Abs (hero.position.x - transform.position.x) > range && !breaked)
				Destroy (gameObject);
		} else {
			if (Mathf.Abs (hero.position.y - transform.position.y) > cabin_range && !breaked)
				BreakDown ();
		}
	}

	// Collision detection
	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag == "Enemy") {
			Enemy enemy = collider.gameObject.GetComponent<Enemy> ();
			if (enemy.Hit (enemy_damage, boss_damage))
				BreakDown ();
		} else if (collider.tag == "Ground" || collider.tag == "Water") {
			GetComponent<Rigidbody2D>().isKinematic = true;
			BreakDown ();
		}
	}

	// Break down the weapon
	private void BreakDown() {
		if (breaked)
			return;

		breaked = true;

		if (life_time > 0) {
			GetComponent<Animator> ().SetBool ("is_collision", true);
			Scheduler.Instance.Add (new Schedule (life_time, false, new Notifier(NotifyDestroy)));
		} else {
			Destroy (gameObject);
			Explode (gameObject.transform, is_cabin);
		}
	}

	// Notifications
	public void NotifyDestroy(object o) {
		Destroy (gameObject);
	}

	// Explode
	private static GameObject m_blow = null;
	public static void Explode(Transform where, bool is_cabin) {
		if (m_blow == null)
			m_blow = (GameObject) Resources.Load("Prefabs/Blow", typeof(GameObject));
		GameObject obj = (GameObject) Instantiate (m_blow, new Vector3 (where.position.x, where.position.y, m_blow.transform.position.z), Quaternion.identity);
		obj.GetComponent<Animator> ().SetBool ("is_cabin", is_cabin);
	}

	// Throw
	private static Rigidbody2D m_stone = null;
	private static Rigidbody2D m_knife = null;
	private static Rigidbody2D m_machete = null;
	private static Rigidbody2D m_axe = null;
	private static Rigidbody2D m_torch = null;
	private static Rigidbody2D m_pitchfork = null;
	public static void Throw(Vector3 from, int direction, WeaponType weapon) {
		Rigidbody2D prefab = null;
		Rigidbody2D obj;

		switch (weapon) {
		case WeaponType.Stone:
			if (m_stone == null)
				m_stone = (Rigidbody2D) Resources.Load("Prefabs/Stone Fly", typeof(Rigidbody2D));
				prefab = m_stone;
			break;
		case WeaponType.Knife:
			if (m_knife == null)
				m_knife = (Rigidbody2D) Resources.Load("Prefabs/Knife Fly", typeof(Rigidbody2D));
			prefab = m_knife;
			break;
		case WeaponType.Machete:
			if (m_machete == null)
				m_machete = (Rigidbody2D) Resources.Load("Prefabs/Machete Fly", typeof(Rigidbody2D));
			prefab = m_machete;
			break;
		case WeaponType.Axe:
			if (m_axe == null)
				m_axe = (Rigidbody2D) Resources.Load("Prefabs/Axe Fly", typeof(Rigidbody2D));
			prefab = m_axe;
			break;
		case WeaponType.Torch:
			if (m_torch == null)
				m_torch = (Rigidbody2D) Resources.Load("Prefabs/Fire Fly", typeof(Rigidbody2D));
			prefab = m_torch;
			break;
		default:
			break;
		}

		if (weapon != WeaponType.No) {
			float x_component = (direction != 0) ? prefab.GetComponent<Weapon>().horizontal_x_component : prefab.GetComponent<Weapon>().vertical_x_component;
			float y_component = (direction != 0) ? prefab.GetComponent<Weapon>().horizontal_y_component : prefab.GetComponent<Weapon>().vertical_y_component;
			float x_offset = (direction != 0) ? prefab.GetComponent<Weapon>().horizontal_x_offset : prefab.GetComponent<Weapon>().vertical_x_offset;
			float y_offset = (direction != 0) ? prefab.GetComponent<Weapon>().horizontal_y_offset : prefab.GetComponent<Weapon>().vertical_y_offset;

			if (direction > 0) {
				obj = (Rigidbody2D) Instantiate (prefab, new Vector3 (from.x + x_offset, from.y + y_offset, prefab.transform.position.z), Quaternion.identity);
			} else if (direction < 0) {
				obj = (Rigidbody2D) Instantiate (prefab, new Vector3 (from.x - x_offset, from.y + y_offset, prefab.transform.position.z), Quaternion.identity);
				x_component = -x_component;
			} else {
				obj = (Rigidbody2D) Instantiate (prefab, new Vector3 (from.x + x_offset, from.y + y_offset, prefab.transform.position.z), Quaternion.identity);
				obj.GetComponent<Rigidbody2D>().isKinematic = true;
				Weapon obj_weapon = obj.GetComponent<Weapon>();
				obj_weapon.is_cabin = true;
				obj_weapon.life_time = 0.0f;
			}

			obj.GetComponent<Animator>().SetInteger("direction", direction);

			if (obj.isKinematic == true)
				obj.velocity = new Vector2 (x_component * State.Instance.hero.throw_scale, y_component);
			else
				obj.AddForce (new Vector2 (x_component * State.Instance.hero.throw_scale, y_component));
		}
	}
}
