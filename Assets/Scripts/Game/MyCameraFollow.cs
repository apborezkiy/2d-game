﻿using UnityEngine;
using System;

public class MyCameraFollow : MonoBehaviour
{
	// references
	public Transform target;

	// settings
	private const float m_screen_height = 224.0f;
	private const float m_screen_width = 256.0f;
	private const float m_area_x = 12.0f;
	private const float disable_time = 0.25f;
	public bool m_movement_capture = true;
	public bool m_is_enabled = true;

	// variables
	private float x;
	
	// Use this for initialization
	private void Start()
	{
		x = target.position.x;
		transform.position = new Vector3 (target.position.x, transform.position.y, transform.position.z);
	}

	// Update is called once per frame
	private void Update()
	{
		if (!m_is_enabled)
			return;

		bool is_updated = false;

		// vertical step
		float y = transform.position.y;
		float sing_y = Mathf.Sign (target.position.y - y);
		float delta_y = Mathf.Abs (target.position.y - y);
		if (delta_y >= m_screen_height) {
			y += ((int)Mathf.Round(delta_y / m_screen_height)) * m_screen_height * sing_y;
			transform.position = new Vector3 (transform.position.x, y, transform.position.z);
			is_updated = true;
		}

		// horizontal
		if (m_movement_capture == true) {
			// flow
			if (Mathf.Abs(x - target.position.x) > m_area_x) {
				x = target.position.x + m_area_x * Mathf.Sign(x - target.position.x);
				transform.position = new Vector3 (x, transform.position.y, transform.position.z);
			}
		} else {
			// step
			if (x != target.position.x) {
				x = target.position.x;
				float delta_x = Mathf.Abs (x - transform.position.x);
				if (delta_x >= m_screen_width || is_updated == true) {
					float look_x = ((int)Mathf.Round(x / m_screen_width)) * m_screen_width;
					transform.position = new Vector3 (look_x, transform.position.y, transform.position.z);
				}
			}
		}
	}

	// Look at specified location
	public void LookAt(float x) {
		this.x = x;
		transform.position = new Vector3 (x, transform.position.y, transform.position.z);
	}

	// Disable during specified time
	public void DisableDuring() {
		GL.Clear (false, true, Color.black);
		GetComponent<Camera> ().enabled = false;
		Scheduler.Instance.Add (new Schedule (disable_time, false, new Types.Notifier (Enabled)));
	}

	// Notifications
	public void Enabled(object unused = null) {
		GetComponent<Camera> ().enabled = true;
	}
}
