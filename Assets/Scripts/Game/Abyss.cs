﻿using UnityEngine;
using System.Collections;

public class Abyss : MonoBehaviour {

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

	// Hero falls to abyss
	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.name == "Hero") {
			GameObject.Find("Main Camera").GetComponent<MyCameraFollow>().m_is_enabled = false;
			if (State.Instance.hero.health > 0) {
				collider.GetComponent<MyCharacterController> ().Hit (State.Instance.hero.health, true);
				Audio.PlaySong(Audio.Tracks.Abyss);
			}
		}
	}
}