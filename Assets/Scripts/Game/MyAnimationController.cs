﻿using UnityEngine;
using System.Collections;

public class MyAnimationController : MonoBehaviour {
	// type defenitions
	public delegate void MyNotify();

	// settings
	public float frequency;

	// variables
	private float twinkle_frequ_timer = 0.0f;
	private float twinkle_timer = 0.0f;
	private float switch_timer = 0.0f;
	private string switch_variable;
	private bool switch_value;
	private MyNotify twinkle_notifier = null;
	private MyNotify switch_notifier = null;
	private float real_z;

	// Use this for initialization
	void Start () {
		real_z = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		if (twinkle_timer > 0) {
			twinkle_timer -= Time.deltaTime;
			if (twinkle_frequ_timer > 0) {
				twinkle_frequ_timer -= Time.deltaTime;
				if (twinkle_frequ_timer <= 0) {
					twinkle_frequ_timer = frequency;
					float z = (transform.position.z == CameraControl.max_z) ? real_z : CameraControl.max_z;
					transform.position = new Vector3 (transform.position.x, transform.position.y, z);
				}
			}
			if (twinkle_timer <= 0) {
				twinkle_timer = 0.0f;
				twinkle_frequ_timer = 0.0f;
				transform.position = new Vector3 (transform.position.x, transform.position.y, real_z);
				if (twinkle_notifier != null) {
					MyNotify notifier = twinkle_notifier;
					twinkle_notifier = null;
					notifier();
				}
			}
		}

		if (switch_timer > 0) {
			switch_timer -= Time.deltaTime;
			if (switch_timer <= 0) {
				switch_timer = 0.0f;
				gameObject.GetComponent<Animator> ().SetBool (switch_variable, switch_value);
				if (switch_notifier != null) {
					MyNotify notifier = switch_notifier;
					switch_notifier = null;
					notifier();
				}
			}
		}
	}

	// Twinkle animations
	public void Twinkle(float time, MyNotify notifier = null) {
		twinkle_timer = time;
		twinkle_frequ_timer = frequency;
		twinkle_notifier = notifier;
	}

	// Predefined animations
	public void SwitchAfter(string variable, bool value, float time, MyNotify notifier = null) {
		switch_variable = variable;
		switch_value = value;
		switch_timer = time;
		switch_notifier = notifier;
	}

	public void Switch(string variable, bool value) {
		gameObject.GetComponent<Animator> ().SetBool (variable, value);
	}
}
