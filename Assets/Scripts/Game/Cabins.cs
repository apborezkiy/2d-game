﻿using UnityEngine;
using System.Collections.Generic;
using Types;

public class Cabins : MonoBehaviour {
	// type definitions
	private class TransitionInfo {
		public Location location;
		public int rotation;
		public int src_layer;
		public int dst_layer;

		// constructor
		public TransitionInfo(Location location, int rotation, int src_layer, int dst_layer) {
			this.location = location;
			this.rotation = rotation;
			this.src_layer = src_layer;
			this.dst_layer = dst_layer;
		}
	};

	private class CabinInfo {
		public uint ordinal;
		public uint capacity;
		public string title;

		public CabinInfo(uint ordinal, uint capacity, string title = "") {
			this.ordinal = ordinal;
			this.capacity = capacity;
			this.title = title;
		}
	};

	public class InterierElement {
		public Spawn spawn;
		public int cabin;
		public int rotation;
		public int layer;
		public string text;
		public int x_offset;
		public bool is_active = true;
		private bool is_created = false;
		
		public InterierElement(Spawn spawn, int cabin, int rotation, int layer, string text = "", int x_offset = 0) {
			this.spawn = spawn;
			this.cabin = cabin;
			this.rotation = rotation;
			this.layer = layer;
			this.text = text;
			this.x_offset = x_offset;
		}

		public bool Create(Respawn respawn, float x, float y) {
			switch (spawn) {
			case Spawn.Letter:
				x -= 33f;
				y -= 41f;
				break;
			case Spawn.MacheteInHouse:
				x += 33;
				y -= 41;
				break;
			case Spawn.AxeInHouse:
				x += 33;
				y -= 41;
				break;
			case Spawn.FireInHouse:
				x += 33;
				y -= 41;
				break;
			case Spawn.FireInFireplace:
				x += 1;
				y -= 11;
				break;
			}

			is_created = true;
			respawn.CreateSpawn(spawn, x + x_offset, y);

			if (spawn == Spawn.Boy || spawn == Spawn.Girl || spawn == Spawn.Kid)
				Talks.Allow (text, State.Instance.hero.name);

			return is_created;
		}

		public bool Delete(Respawn respawn) {
			if (is_created) {
				is_created = false;
				respawn.DeleteSpawn(spawn);
			}

			return true;
		}
	};

	// variables
	private List<CabinInfo> cabins = new List<CabinInfo>();
	private List<TransitionInfo> transitions = new List<TransitionInfo>();
	public List<InterierElement> elements = new List<InterierElement>();
	public List<InterierElement> actions = new List<InterierElement>();
	public System.Random random = new System.Random();

	// constructor
	public Cabins() {
		// init cabins
		cabins.Add(new CabinInfo(1, 1, ""));
		cabins.Add(new CabinInfo(2, 1, ""));
		cabins.Add(new CabinInfo(3, 1, ""));
		cabins.Add(new CabinInfo(4, 1, ""));
		cabins.Add(new CabinInfo(5, 2, ""));
		cabins.Add(new CabinInfo(6, 1, ""));
		cabins.Add(new CabinInfo(7, 1, ""));
		cabins.Add(new CabinInfo(8, 1, "GEORGE HOUSE"));
		cabins.Add(new CabinInfo(9, 1, ""));

		// init static objects
		// - fireplaces
		actions.Add (new InterierElement (Spawn.FireInFireplace, 11, 0, 4));
		actions.Add (new InterierElement (Spawn.FireInFireplace, 12, 0, 4));
		actions.Add (new InterierElement (Spawn.FireInFireplace, 13, 0, 4));
		actions.Add (new InterierElement (Spawn.FireInFireplace, 14, 0, 4));
		actions.Add (new InterierElement (Spawn.FireInFireplace, 15, 0, 4));
		actions.Add (new InterierElement (Spawn.FireInFireplace, 16, 0, 4));
		actions.Add (new InterierElement (Spawn.FireInFireplace, 17, 0, 4));
		// - tambour doors
		actions.Add (new InterierElement (Spawn.Door, 21, 0, 1));
		actions.Add (new InterierElement (Spawn.Door, 22, 0, 1));
		actions.Add (new InterierElement (Spawn.Door, 23, 0, 1));

		// init transitions
		// - small house
		transitions.Add (new TransitionInfo (Location.SmallHouse, 270, 1, 2));
		transitions.Add (new TransitionInfo (Location.SmallHouse, 90, 2, 1));
		transitions.Add (new TransitionInfo (Location.SmallHouse, 180, 1, 0));
		// - big house
		transitions.Add (new TransitionInfo (Location.BigHouse, 0, 1, 2));
		transitions.Add (new TransitionInfo (Location.BigHouse, 0, 2, 3));
		transitions.Add (new TransitionInfo (Location.BigHouse, 180, 3, 2));
		transitions.Add (new TransitionInfo (Location.BigHouse, 180, 2, 1));
		transitions.Add (new TransitionInfo (Location.BigHouse, 180, 1, 0));
		transitions.Add (new TransitionInfo (Location.BigHouse, 90, 3, 4));
		transitions.Add (new TransitionInfo (Location.BigHouse, 270, 4, 3));
		// - house in the woods
		transitions.Add (new TransitionInfo (Location.WoodsHouse, 270, 2, 3));
		transitions.Add (new TransitionInfo (Location.WoodsHouse, 90, 3, 2));
		transitions.Add (new TransitionInfo (Location.WoodsHouse, 180, 2, 1));
		transitions.Add (new TransitionInfo (Location.WoodsHouse, 0, 1, 2));
		transitions.Add (new TransitionInfo (Location.WoodsHouse, 180, 1, 0));
		// - house in the cave
		transitions.Add (new TransitionInfo (Location.CaveHouse, 180, 2, 0));
		transitions.Add (new TransitionInfo (Location.CaveHouse, 0, 1, 2));
		transitions.Add (new TransitionInfo (Location.CaveHouse, 180, 1, 0));

		//
		elements.Add (new InterierElement (Spawn.Letter, 5, 0, 1, "BEWARE\r\nKIDS..."));
		elements.Add (new InterierElement (Spawn.AxeInHouse, 5, 0, 1));
		elements.Add (new InterierElement (Spawn.MacheteInHouse, 5, 90, 1));
		elements.Add (new InterierElement (Spawn.FireInHouse, 5, 180, 1));
	}

	// Use this for initialization
	void Awake() {
		DontDestroyOnLoad (transform.root.gameObject);
	}

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Update aborigines' location
	public void LocateAborigines(int house) {
		Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();
		System.Predicate<InterierElement> is_human = e => e.spawn == Spawn.Boy || e.spawn == Spawn.Girl || e.spawn == Spawn.Kid;
		elements.RemoveAll (e => is_human(e) && e.cabin == house && e.Delete(respawn));
		foreach (Hero hero in State.Instance.heroes) {
			if (hero.house == house && hero.health > 0 && hero.name != State.Instance.hero.name) {
				int [,] location = { { 0, 1 }, { 90, 1 }, { 180, 2 } }; /* {rotation, layer} */
				int count = elements.FindAll(e => is_human(e) && e.cabin == hero.house).Count;
				elements.Add(new InterierElement(hero.is_woman ? Spawn.Girl : Spawn.Boy, hero.house, location[count, 0], location[count, 1], hero.name));
			}
		}

		if (house == State.kids_house) {
			const int x_offset = 14;
			int kids = Mathf.Min (State.Instance.kids, 5);
			for (int i = -1; i < kids - 1; ++i)
				elements.Add (new InterierElement (Spawn.Kid, State.kids_house, 0, 1, "KIDS", x_offset * i));
		}
	}

	// Get hero located
	public Passport GetHeroLocated(int cabin, int layer, int rotation) {
		if (elements.Exists (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.spawn == Spawn.Girl || e.spawn == Spawn.Boy))) {
			InterierElement element = elements.Find (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.spawn == Spawn.Girl || e.spawn == Spawn.Boy));
			return Hero.GetPassport(element.text);
		} else {
			return Passport.No;
		}
	}

	// Current cabin's layer
	public int GetLayer(Location location, int layer, int rotation) {
		if (transitions.Exists (info => info.location == location && info.src_layer == layer && info.rotation == rotation))
			return transitions.Find (info => info.location == location && info.src_layer == layer && info.rotation == rotation).dst_layer;
		else
			return layer;
	}

	// Current layer's and rotation's interier
	public void LoadInterier(Vector3 target, int cabin, int layer, int rotation) {
		Talks.Deny ();

		Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();
		elements.ForEach (e => e.Delete (respawn));

		elements.FindAll (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.Create (respawn, target.x, target.y)));
	}

	// Take the first item in the current location
	public Spawn Take (int cabin, int layer, int rotation) {
		if (elements.Exists (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation))) {
			Respawn respawn = GameObject.Find ("Respawn").GetComponent<Respawn> ();
			InterierElement element = elements.Find (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation));
			Spawn spawn = element.spawn;
			if (spawn == Spawn.Letter)
				GameObject.Find ("Letter Text").GetComponent<TextMesh>().text = element.text;
			element.Delete (respawn);
			elements.Remove (element);
			return spawn;
		} else {
			return Spawn.Empty;
		}
	}

	// Use the first action in the current location
	public delegate bool ActionFilter(Spawn action);
	public Spawn Use (int cabin, int layer, int rotation, ActionFilter filter) {
		if (actions.Exists (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.is_active))) {
			InterierElement action = actions.Find (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation));
			Spawn spawn = action.spawn;
			if (filter (spawn)) {
				actions.Remove (action);
				action.is_active = false;
				actions.Add (action);
				return spawn;
			}
		}

		return Spawn.Empty;
	}

	// Doors
	public DoorState GetDoor(int cabin, int layer, int rotation) {
		if (actions.Exists (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.spawn == Spawn.Door))) {
			InterierElement action = actions.Find (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.spawn == Spawn.Door));
			return action.is_active ? DoorState.Locked : DoorState.Opened;
		} else {
			return DoorState.Absent;
		}
	}
	
	public DoorState OpenDoor(int cabin, int layer, int rotation) {
		if (actions.Exists (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.spawn == Spawn.Door))) {
			InterierElement action = actions.Find (e => (e.cabin == cabin) && (e.layer == layer) && (e.rotation == rotation) && (e.spawn == Spawn.Door));
			actions.Remove(action);
			action.is_active = false;
			actions.Add(action);
			return DoorState.Opened;
		} else {
			return DoorState.Absent;
		}
	}

	// Random cabin location
	public void LocateRandom(Location location, out int layer, out int rotation) {
		rotation = (random.Next() % 4) * 90;
		switch (location) {
		case Location.SmallHouse:
			layer = (random.Next() % 2) + 1;
			break;
		case Location.BigHouse:
			layer = (random.Next() % 4) + 1;
			break;
		case Location.WoodsHouse:
			layer = (random.Next() % 2) + 2;
			break;
		default:
			rotation = 0;
			layer = 0;
			break;
		}
	}

	// Cabins instance
	private static Cabins instance = null;
	public static Cabins Instance {
		get {
			if (instance != null) {
				return instance;
			} else {
				GameObject original = GameObject.Find ("Cabins Reference");
				instance = original.AddComponent<Cabins>();
				return instance;
			}
		}
	}
}
