﻿using UnityEngine;
using System.Collections;

public class ActiveKnife : MonoBehaviour {
	public int speed;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (-speed, -speed);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D(Collider2D obj) {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 0f);
	}
}
