﻿using UnityEngine;
using System.Collections;

public class Childrens : MonoBehaviour {
	private TextMesh mesh;

	public bool is_digit = false;

	// Use this for initialization
	void Start () {
		mesh = GetComponent<TextMesh> ();
		if (is_digit) {
			State.kids_changed += DigitChanged;
			DigitChanged();
		} else {
			State.kids_changed += FacesChanged;
			FacesChanged();
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Notifications
	private void DigitChanged() {
		mesh.text = State.Instance.kids.ToString ();
	}

	private void FacesChanged() {
		int kids = Mathf.Min(State.Instance.kids, 5);
		mesh.text = "";
		for (int i = kids; i != 0; --i)
			mesh.text += "|";
	}
}
