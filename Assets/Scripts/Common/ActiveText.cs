﻿using UnityEngine;
using System;
using System.Collections;
using Types;

public class ActiveText : MonoBehaviour {
	// scene object references
	private TextMesh source;

	// finish typing notification
	private Notifier notifier;
	
	// settings
	private const float start = 0.5f; /* delay before start typing */
	private const float appearence = 0.07f; /* characters appearence interval */

	// variables
	private float timer = 0;
	private String text = ""; /* message */
	private String buffer = ""; /* typing buffer */

	// Use this for initialization
	void Start () {
		LoadText ();
	}

	// Function is called once per frame
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;
			if (timer <= 0) {
				timer = 0;
				if (buffer.Length > 1) {
					timer = appearence;
					source.text += buffer.Remove (1);
					buffer = buffer.Substring (1);
					// play letter song
					Audio.PlaySong(Audio.Tracks.Letter);
				} else if (buffer.Length == 1) {
					source.text += buffer;
					buffer = "";
					// notify typing is finished
					if (notifier != null)
						notifier.Start();
				}
			}
		}
	}

	// Update text
	public void LoadText() {
		source = GetComponent<TextMesh> ();
		text = source.text;
		source.text = "";
	}

	// Type the text
	public void Type(Notifier notifier = null) {
		this.notifier = notifier;
		this.buffer = this.text;
		this.source.text = "";
		// delay before typing
		Scheduler.Instance.Add(new Schedule(start, false, new Notifier(NotifyStart)));
	}
	
	// Notifications
	public void NotifyStart(object arg) {
		// start type
		timer = appearence;
	}

	// text contents
	public static string[] messages = {
		/* warnings and talks */
		"YOU CAN NOT\nGET IN\nWITHOUT A KEY.", /* 0 */
		"YOU WIN\nFOR NOW.", /* 1 */
		"THANK YOU!" /* 2 */
		/* letters */
	};
}