﻿using UnityEngine;
using System.Collections;
using Types;

public class AquaZombie : Enemy {
	// settings
	private const float jump_force = 12000.0f;
	private const float emerge_time = 0.2f; /* from creating object to applying a force */
	private const float deviation = 60.0f; /* from hero to change direction */
	private const float distance = 140.0f; /* of enemy existence */
	
	// variables
	private bool is_emerge = false;
	
	// constructor
	private AquaZombie() : base(Spawn.AquaZombie) {
	}
	
	// Use this for initialization
	protected override void Start () {
		base.Start ();
		body.isKinematic = true;
		my_animation.SwitchAfter ("emerge", true, emerge_time, NotifyEmerge);
		Audio.PlaySong (Audio.Tracks.Splash);
	}
	
	// Update is called once per frame
	void Update () {
		if (lifes > 0) {
			// select looking direction
			if ((hero.position.x + deviation < transform.position.x && direction == true) || (hero.position.x - deviation > transform.position.x && direction == false))
				direction = direction ? false : true;
		}

		// destroy when out of enemy existence zone
		if (Mathf.Abs (hero.position.x - transform.position.x) > distance)
			base.NotifyDie ();
	}
	
	// Weapon hits to the enemy
	public override bool Hit(int damage, int aux_damage) {
		return base.Hit(damage, damage);
	}

	// Diving detection
	protected override void OnTriggerEnter2D(Collider2D collider) {
		base.OnTriggerEnter2D (collider);
		if (is_emerge == true && collider.tag == "Ground") {
			body.isKinematic = true;
			body.velocity = new Vector2(0.0f, 0.0f);
			my_animation.Switch("dive", true);
			my_animation.SwitchAfter ("dive", true, emerge_time, NotifyDive);
			Audio.PlaySong(Audio.Tracks.SplashDown);
		}
	}

	// Notifications
	public void NotifyEmerge() {
		is_emerge = true;
		body.isKinematic = false;
		body.AddForce(new Vector2(0.0f, jump_force));
	}

	public void NotifyDive() {
		base.NotifyDie ();
	}

	public override void NotifyDie() {
	}
}