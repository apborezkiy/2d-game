﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Types;

public class Talks : MonoBehaviour {
	private static TextMesh text;

	private class Talk
	{
		public string teller;
		public string listener;
		public string text;

		public Talk(string teller, string listener, string text) {
			this.teller = teller;
			this.listener = listener;
			this.text = text;
		}
	};

	// settings
	private const float pause_before = 1.0f;
	private const float pause_after = 1.0f;
	private const float duration = 3.0f;

	// variables
	private static string teller = "";
	private static string listener = "";
	private static bool allowed;
	private static List<Talk> talks = new List<Talk>();

	// Use this for initialization
	void Start () {
		text = GameObject.Find ("Talk Text").GetComponent<TextMesh>();
		text.text = "";
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Create talk
	public static void Create(string name_src, string name_dst, string text) {
		talks.Add (new Talk(name_src, name_dst, text));
	}

	public static void CreateAny(string name_src, string text) {
		Create (name_src, "", text);
	}

	// Allow talks
	public static void Allow(string name_src, string name_dst) {
		teller = name_src;
		listener = name_dst;
		allowed = true;

		Tell ();
	}

	// Deny all talks
	public static void Deny() {
		allowed = false;
	}

	// Wait for a pause
	private static void Tell(object unused = null) {
		text.text = "";
		Scheduler.Instance.Add (new Schedule (pause_before, false, new Notifier (DoTell)));
	}

	// Try to tell something
	private static void DoTell(object unused = null) {
		if (allowed && talks.Exists (e => (e.listener == listener || e.listener == "") && e.teller == teller)) {
			Talk talk = talks.Find (e => (e.listener == listener || e.listener == "") && e.teller == teller);
			text.text = talk.text;
			talks.Remove(talk);

			Scheduler.Instance.Add (new Schedule (duration, false, new Notifier (Tell, pause_after)));
		}
	}
}