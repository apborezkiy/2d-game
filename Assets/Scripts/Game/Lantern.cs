﻿using UnityEngine;
using System.Collections;
using Types;

public class Lantern : MonoBehaviour {
	// lantern event
	public delegate void LanternTurned(bool state);
	public static event LanternTurned OnLanternTurned;
	
	// variables
	private static uint timer_id = 0;
	private float original_z;

	// Use this for initialization
	void Start () {
		OnLanternTurned += LanternIsTurned;
		original_z = transform.position.z;
		LanternIsTurned (false);
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Turn lantern
	public static void Turn() {

		State.Instance.hero.is_lantern_turned = State.Instance.hero.is_lantern_turned ? false : true;

		if (State.Instance.hero.is_lantern_turned && State.Instance.hero.lanterns > 0) {
			timer_id = Scheduler.Instance.Add (new Schedule (1, true, new Notifier (Tick)));
		} else {
			State.Instance.hero.is_lantern_turned = false;
			if (timer_id != 0) {
				Scheduler.Instance.Del(ref timer_id);
				timer_id = 0;
			}
		}

		OnLanternTurned(State.Instance.hero.is_lantern_turned);
	}

	// Discharge of the battery
	public static void Tick(object unused) {
		if (--State.Instance.hero.lanterns == 0) {
			Turn();
		}
	}

	// Show or hide cave's walls
	private void LanternIsTurned(bool state) {
		if (state) {
			transform.position = new Vector3(transform.position.x, transform.position.y, original_z);
		} else {
			transform.position = new Vector3(transform.position.x, transform.position.y, CameraControl.max_z);
		}
	}
}