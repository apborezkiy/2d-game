﻿using UnityEngine;
using System.Collections;

public class LifesBar : MonoBehaviour {
	private TextMesh mesh;

	// variables
	public Passport hero = Passport.No;
	public bool is_hero_oriented = true;
	
	// Use this for initialization
	void Start () {
		State.health_changed += LifesChanged;
		State.hero_changed += LifesChanged;
		mesh = GetComponent<TextMesh> ();
		LifesChanged ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Notifications
	private void LifesChanged() {
		int health;

		if (hero == Passport.No)
			health = State.Instance.hero.health;
		else if (is_hero_oriented)
			health = State.Instance.GetHero (Hero.GetName (hero)).health;
		else
			health = (int)System.Math.Ceiling(State.Instance.GetBoss (hero).health / 10.0f);

		health = Mathf.Max (0, health);

		mesh.text = "";
		for (int i = health; i != 0; --i)
			mesh.text += "|";
	}
}