﻿using UnityEngine;
using System.Collections;
using Types;

public class Detector : MonoBehaviour {
	public enum DetectorType { hero, kids };

	// settings
	public DetectorType kind;

	// variables
	float z;
	const float twinkle = 0.05f;
	const uint signal_twinkles = 200;
	uint twinkles = 0;
	uint timer_id = 0;

	// Use this for initialization
	void Start () {
		z = gameObject.transform.position.z;
		State.detector_changed += DetectorChanged;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Notifications
	private void DetectorChanged() {
		if (State.Instance.hero.house != 0) {
			if (timer_id != 0) {
				Scheduler.Instance.Del (ref timer_id);
				gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
			}
		} else {
			if (kind == DetectorType.kids) {
				// kids
				if (State.Instance.is_kids_detector) {
					if (timer_id == 0) {
						timer_id = Scheduler.Instance.Add (new Schedule (twinkle, true, new Notifier (Twinkle)));
						Audio.PlaySong (Audio.Tracks.Alert);
					}
				} else {
					if (timer_id != 0) {
						Scheduler.Instance.Del (ref timer_id);
						gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
					}
				}
			} else if (kind == DetectorType.hero) {
				// hero
				if (State.Instance.is_hero_detector) {
					if (timer_id == 0) {
						timer_id = Scheduler.Instance.Add (new Schedule (twinkle, true, new Notifier (Twinkle)));
						Audio.PlaySong (Audio.Tracks.Alert);
					}
				} else {
					if (timer_id != 0) {
						Scheduler.Instance.Del (ref timer_id);
						gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
					}
				}
			}
		}
	}

	public void Twinkle(object unsed = null) {
		// alert
		if (++twinkles >= signal_twinkles) {
			Audio.PlaySong(Audio.Tracks.Alert);
			twinkles = 0;
		}
		// twinkle
		if (gameObject.transform.position.z != z) {
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
		} else {
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, CameraControl.max_z);
		}
	}
}
