﻿using System;
using System.Collections;
using Types;

public class Boss {
	// personal information
	public Passport passport;
	public String name;

	// location information
	public int house;
	public int cabin_layer;
	public int cabin_rotation;
	public Location location;

	// health information
	public readonly int max_health;
	private int _health;
	public int health {
		get { return _health; }
		set {
			_health = Math.Min(value, max_health);
			State.HealthChanged();
		}
	}

	// physical information
	public WeaponType weapon = WeaponType.No;
	public float walk_speed;
	public float walk_speed_in;
	public const int max_wounds = 100;
	public const int max_blows = 100;
	public const int max_hits = 10;
	public int wounds = 0; /* from hero's weapon */
	public int blows = 0; /* weapon's swings */
	public int hits = 0; /* to the hero */
	public bool is_win = true;

	// time information
	public enum Occasions { Battle, Trap, Meeting };
	public const float max_time = 200.0f;
	private float _last_joke_time = max_time;
	private float _last_battle_time = max_time;
	private float _last_discovery_time = max_time;
	public float last_joke_time {
		get { return Scheduler.Instance.time - _last_joke_time; }
		set { _last_joke_time = value; }
	}
	public float last_battle_time {
		get { return Scheduler.Instance.time - _last_battle_time; }
		set { _last_battle_time = value; }
	}
	public float last_discovery_time {
		get { return Scheduler.Instance.time - _last_discovery_time; }
		set { _last_discovery_time = value; }
	}

	// constructor
	public Boss(Passport passport, int max_health, int house, int cabin_layer, int cabin_rotation, Location location, float walk_speed, float walk_speed_in) {
		this.passport = passport;
		this.name = Hero.GetName (passport);
		this.max_health = max_health;
		this._health = max_health;
		this.house = house;
		this.cabin_layer = cabin_layer;
		this.cabin_rotation = cabin_rotation;
		this.location = location;
		this.walk_speed = walk_speed;
		this.walk_speed_in = walk_speed_in;
	}

	// Prepare to the battle
	public void Reset() {
		wounds = 0;
		blows = 0;
		hits = 0;
	}

	// Sences calculation
	public float CalculateFatigue(Intellect.SenseGetter getter) {
		float [] c = { 0.30f, 0.30f, 0.25f, 0.20f, 0.10f, 0.20f, 0.05f };
		return blows / (c[(int)weapon] * max_blows);
	}

	public float CalculatePain(Intellect.SenseGetter getter) {
		float [] c = { 0.20f, 0.20f, 0.15f, 0.10f, 0.05f, 0.02f, 0.05f };
		return wounds / (c[(int)State.Instance.hero.weapon] * max_wounds);
	}

	public float CalculateSurvival(Intellect.SenseGetter getter) {
		return 1.0f - (health / max_health);
	}

	public float CalculateWin(Intellect.SenseGetter getter) {
		return (is_win ? 0.5f : 0.0f) + (((float)State.Instance.hero.health / (float)Hero.max_health) / 2.0f);
	}

	public float CalculateFailure(Intellect.SenseGetter getter) {
		float [] c = { 0.5f, 0.5f, 0.6f, 0.8f, 1.0f, 1.0f, 1.0f };
		if (blows == hits)
			return (is_win ? 0.0f : 0.5f);
		else
			return (is_win ? 0.0f : 0.5f) + (1.0f - ((hits / (blows - hits)) * c[(int)weapon])) / 2.0f;
	}

	public float CalculateInterest(Intellect.SenseGetter getter) {
		float [] c = { 1.0f, 9.0f, 8.0f };
		return c[(int)State.Instance.weather] * (last_discovery_time / max_time);
	}

	public float CalculateRevenge(Intellect.SenseGetter getter) {
		float [] c = { 0.5f, 0.5f, 0.6f, 0.8f, 1.0f, 1.0f, 1.0f };
		float pain = getter (Intellect.Sences.Pain);
		float failure = getter (Intellect.Sences.Failure);
		float survival = getter (Intellect.Sences.Survival);
		float average = (pain + failure + survival) / 3.0f;
		return c [(int)weapon] * average;
	}

	public float CalculateBoredom(Intellect.SenseGetter getter) {
		float [] c = { 1.0f, 0.9f, 0.8f };
		return ((last_joke_time / max_time + last_battle_time / max_time) / 2.0f) * c[(int)State.Instance.weather];
	}

	public float CalculateRage(Intellect.SenseGetter getter) {
		float [] c = { 0.6f, 0.8f, 1.0f };
		return getter (Intellect.Sences.Revenge) * (1.0f - getter (Intellect.Sences.Boredom)) * c[(int)State.Instance.weather];
	}

	// Update appropriate timer
	public void Occasion(Occasions occasion) {
		switch (occasion) {
		case Occasions.Trap:
			last_joke_time = Scheduler.Instance.time;
			break;
		case Occasions.Battle:
			last_battle_time = Scheduler.Instance.time;
			break;
		case Occasions.Meeting:
			last_discovery_time = Scheduler.Instance.time;
			break;
		default:
			break;
		}
	}

	// Is boss ready to surrender
	public bool IsSurrender() {
		if (CalculatePain (null) >= 1.0f)
			return true;

		return false;
	}
}