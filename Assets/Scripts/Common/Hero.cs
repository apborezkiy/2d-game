﻿using System;
using System.Collections;
using Types;

public enum Passport {
	No = 0,
	George = 1,
	Mark = 2,
	Paul = 3,
	Laura = 4,
	Debbie = 5,
	Crissy = 6,
	Jason = 7,
	Head = 8
};

public enum WeaponType {
	No = 0,
	Stone = 1,
	Knife = 2,
	Machete = 3,
	Axe = 4,
	Torch = 5,
	Pitchfork = 6
};

public class Hero {
	// personal information
	public Passport passport;
	public String name;
	public const int max_health = 32;
	private int _health = max_health;
	public int health {
		get { return _health; }
		set {
			_health = Math.Min(value, max_health);
			State.HealthChanged();
		}
	}
	// location information
	public int house = 0;
	public int cabin_layer = 0;
	public int cabin_rotation = 0;
	public Location location = Location.SmallHouse;
	// inventory information
	private WeaponType _weapon = WeaponType.Stone;
	public WeaponType weapon {
		get { return _weapon; }
		set {
			_weapon = value;
			State.InventoryChanged();
		}
	}
	public const int compote_curing = 5;
	private uint _compotes = 0; /* in pieces */
	public uint compotes {
		get { return _compotes; }
		set {
			_compotes = value;
			State.InventoryChanged();
		}
	}
	private uint _keys = 0; /* in pieces */
	public uint keys {
		get { return _keys; }
		set {
			_keys = value;
			State.InventoryChanged();
		}
	}
	private uint _lighters = 0; /* in pieces */
	public uint lighters {
		get { return _lighters; }
		set {
			_lighters = value;
			State.InventoryChanged();
		}
	}
	public const uint lantern_capacity = 60;
	private uint _lanterns = 0; /* in second */
	public uint lanterns {
		get { return _lanterns; }
		set {
			_lanterns = value;
			State.InventoryChanged();
		}
	}
	// activity information
	public uint kills = 0;
	public uint jumps = 0;
	public uint battles = 0;
	public uint lit = 0;
	// physical information
	public float walk_scale = 1.0f;
	public float jump_scale = 1.0f;
	public float sweam_scale = 1.0f;
	public float throw_scale = 1.0f;
	// cloth information
	public int pallet;
	public bool is_woman;
	// current state information
	public bool is_lantern_turned = false;
	public bool is_action_selector = false;
	private int _action_selector = 0;
	public int action_selector {
		get { return _action_selector; }
		set { _action_selector = Math.Max(Math.Min(value, 4), 0); }
	}
	
	// constructor
	public Hero(Passport passport, int house, int pallet, bool is_woman, float walk_scale, float jump_scale, float sweam_scale, float throw_scale) {
		this.passport = passport;
		this.name = Hero.GetName (passport);
		this.house = house;
		this.walk_scale = walk_scale;
		this.jump_scale = jump_scale;
		this.sweam_scale = sweam_scale;
		this.throw_scale = throw_scale;
		this.pallet = pallet;
		this.is_woman = is_woman;
	}

	// informations
	public static Passport GetPassport(String name) {
		if (name == "George") return Passport.George;
		else if (name == "Mark") return Passport.Mark;
		else if (name == "Paul") return Passport.Paul;
		else if (name == "Laura") return Passport.Laura;
		else if (name == "Debbie") return Passport.Debbie;
		else if (name == "Crissy") return Passport.Crissy;
		else if (name == "Jason") return Passport.Jason;
		else if (name == "Head") return Passport.Head;
		else return Passport.No;
	}

	public static String GetName(Passport passport) {
		switch (passport) {
		case Passport.George: return "George";
		case Passport.Mark: return "Mark";
		case Passport.Paul: return "Paul";
		case Passport.Laura: return "Laura";
		case Passport.Debbie: return "Debbie";
		case Passport.Crissy: return "Crissy";
		case Passport.Jason: return "Jason";
		case Passport.Head: return "Head";
		default: return null;
		}
	}
};