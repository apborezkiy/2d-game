﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour {
	// types definitions
	public enum Tracks
	{
		/* music */
		Indoor = 0, Outdoor = 1, Battle = 2, Win = 3,
		/* songs */
		HeroShoot = 4, Wound = 5,
		Letter = 6, Bonus = 7, Success = 8,
		Pick = 7, Use = 8,
		Jump = 9,
		Die = 10, Surprise = 11,
		GafferShoot = 12, Blow = 12,
		Alert = 13,
		Abyss = 14, Failure = 14,
		Wolf = 15, Fox = 15,
		Splash = 16, Scar = 17,
		AddHealth = 18,
		SubHealth = 19,
		Confuse = 20,
		Disable = 21,
		Enable = 22,
		Shikadi = 23,
        SplashDown = 24,
        AntiConfuse = 25,
		/* aux */
		Empty = 99
	}

	// settings
	public bool is_enabled = true;
	public bool is_user = false;

	// variables
	private const int tracks_count = 25;
	public AudioClip [] tracks_original = new AudioClip [tracks_count];
	public AudioClip [] tracks_user = new AudioClip [tracks_count];
	public AudioSource song_source = new AudioSource();
	public AudioSource music_source = new AudioSource();
	private Tracks current_music = Tracks.Empty;

	// interface
	public static void PlayMusic(Tracks music) {
		if (instance == null || !instance.is_enabled)
			return;

		AudioClip clip = instance.GetActiveTrack (music);
		if ((instance.current_music != music || !instance.music_source.isPlaying) && clip != null) {
			instance.music_source.clip = clip;
			instance.music_source.Play();
			instance.current_music = music;
		}
	}

	public static void StopMusic(Tracks music) {
		if (instance == null || !instance.is_enabled)
			return;

		if (instance.music_source.isPlaying && instance.current_music == music)
			instance.music_source.Stop ();
	}

	public static void StopAllMusic() {
		if (instance == null || !instance.is_enabled)
			return;
		
		instance.music_source.Stop ();
	}

	public static void PlaySong(Tracks track) {
		if (instance == null || !instance.is_enabled)
			return;
		
		AudioClip clip = instance.GetActiveTrack (track);
		if (clip != null)
			instance.song_source.PlayOneShot (clip);
	}

	// auxiliary
	private AudioClip GetActiveTrack(Tracks track) {
		if (is_user && tracks_user [(int)track] != null)
			return tracks_user [(int)track];
		else
			return tracks_original [(int)track];
	}
	
	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad (this);

		// singleton
		instance = this;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Audio instance
	private static Audio instance = null;
	public static Audio Instance {
		get { return instance; }
	}
}