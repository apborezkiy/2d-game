﻿using UnityEngine;
using System;
using System.Collections;
using Types;

public class Scenary : MonoBehaviour {
	public MonoBehaviour main_camera;
	public MonoBehaviour face_picker;
	public MonoBehaviour trip_mesh_1;
	public MonoBehaviour gameover_mesh_1;
	public MonoBehaviour gameover_mesh_2;
	public MonoBehaviour gameover_mesh_3;
	public MonoBehaviour gameover_mesh_4;
	public MonoBehaviour win_mesh_1;
	public MonoBehaviour win_mesh_2;

	private CameraControl view;
	private FacePicker picker;

	// variables
	bool is_keyboard = true;

	// scenary
	enum MenuState { Idle = 0, Twinkle = 1, Title = 2, ResetToIdle = 3, Copyright = 4, Map = 5, Trip = 6, Game = 7, GameOver = 8, Exit = 9, Win = 10, Reset = 11 };
	MenuState _state;
	MenuState state {
		get { return _state; }
		set {
			_state = value;
			switch (value) {
			case MenuState.Idle:
				Scheduler.Instance.Add(new Schedule(3.0f, false, new Notifier(NotifyNextState, _state)));
				break;
			case MenuState.Twinkle:
				view.twinkle = true;
				Scheduler.Instance.Add(new Schedule(2.0f, false, new Notifier(NotifyNextState, _state)));
				break;
			case MenuState.Title:
				view.twinkle = false;
				view.screen = 1;
				Audio.PlaySong(Audio.Tracks.Surprise);
				Scheduler.Instance.Add(new Schedule(6.0f, false, new Notifier(NotifyNextState, _state)));
				break;
			case MenuState.ResetToIdle:
				Application.LoadLevel(Application.loadedLevelName);
				break;
			case MenuState.Copyright:
				view.screen = 2;
				Audio.PlayMusic(Audio.Tracks.Indoor);
				break;
			case MenuState.Map:
				if (State.Instance.IsAnyHeroAlive() && State.Instance.kids > 0) {
					Audio.PlayMusic(Audio.Tracks.Indoor);
					view.screen = 3;
					picker.is_enabled = true;
				} else if (State.Instance.kids > 0) {
					Audio.StopAllMusic();
					view.screen = 5;
					is_keyboard = false;
					_state = MenuState.GameOver;
					gameover_mesh_1.GetComponent<ActiveText>().Type(new Notifier(NotifyGameOver1));
				} else {
					Audio.StopAllMusic();
					view.screen = 7;
					is_keyboard = false;
					_state = MenuState.GameOver;
					gameover_mesh_3.GetComponent<ActiveText>().Type(new Notifier(NotifyGameOver2));
				}
				break;
			case MenuState.Trip:
				if (State.Instance.is_trip) {
					view.screen = 4;
					is_keyboard = false;
					trip_mesh_1.GetComponent<ActiveText>().Type(new Notifier(NotifyKeyboard));
					Scheduler.Instance.Add(new Schedule(4.0f, false, new Notifier(NotifyNextState, _state)));
				} else
					state++;
				break;
			case MenuState.Game:
				Application.LoadLevel("Game");
				break;
			case MenuState.Exit:
				Application.Quit();
				break;
			case MenuState.Win:
				Audio.StopAllMusic();
				Audio.PlayMusic(Audio.Tracks.Win);
				view.screen = 6;
				is_keyboard = false;
				win_mesh_1.GetComponent<ActiveText>().Type(new Notifier(NotifyTheEnd));
				break;
			case MenuState.Reset:
				Application.Quit();
				break;
			default:
				break;
			}
		}
	}

	// Use this for initialization
	void Start() {
		view = main_camera.gameObject.GetComponent<CameraControl> ();
		picker = face_picker.gameObject.GetComponent<FacePicker> ();
		if (State.Instance.is_the_end) {
			state = MenuState.Win;
		} else {
			if (State.Instance.is_title == true)
				state = MenuState.Idle;
			else
				state = MenuState.Map;
		}
	}

	// Function is called once per frame
	void Update () {
		// input
		if (Input.GetKeyDown ("return") && is_keyboard == true) {
			if (state < MenuState.Title)
				state = MenuState.Title;
			else if (state < MenuState.Copyright)
				state = MenuState.Copyright;
			else if (state < MenuState.Trip)
				state++;
		}
	}

	// Notifications
	public void NotifyNextState(object current) {
		if (state < (MenuState)((int)current + 1))
			state++;
	}

	public void NotifyKeyboard(object arg) {
		is_keyboard = true;
	}
	
	public void NotifyGameOver1(object arg) {
		gameover_mesh_2.GetComponent<ActiveText>().Type(new Notifier(NotifyKeyboard));
		Scheduler.Instance.Add(new Schedule(6.0f, false, new Notifier(NotifyNextState, state)));
	}

	public void NotifyGameOver2(object arg) {
		gameover_mesh_4.GetComponent<ActiveText>().Type(new Notifier(NotifyKeyboard));
		Scheduler.Instance.Add(new Schedule(6.0f, false, new Notifier(NotifyNextState, state)));
	}

	public void NotifyTheEnd(object arg) {
		win_mesh_2.GetComponent<ActiveText>().Type(new Notifier(NotifyKeyboard));
		Scheduler.Instance.Add(new Schedule(8.0f, false, new Notifier(NotifyNextState, state)));
	}
}
