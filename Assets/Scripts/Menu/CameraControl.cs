﻿using UnityEngine;
using System;
using System.Collections;

public class CameraControl : MonoBehaviour {
	private Camera main_camera;

	// settings
	public static float max_z = 99.0f; /* farest position */
	public static float min_z = -1.0f; /* nearest position */
	public int step = 300; /* adjacent screen interval */
	public float delay = 0.03f; /* camera color change interval */

	// twinkle
	public bool twinkle {
		set {
			if (value == true)
				timer = delay;
			else
			{
				timer = 0;
				main_camera.backgroundColor = Color.black;
			}
		}
	}

	// current screen
	int _screen = 0;
	public int screen {
		get { return _screen; }
		set {
			_screen = value;
			int x = _screen * step;
			Vector3 look = GetComponent<Camera>().gameObject.transform.position;
			GetComponent<Camera>().gameObject.transform.position = new Vector3 (x, look.y, look.z);
		}
	}

	// variables
	float timer = 0.0f;

	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		main_camera = GetComponent<Camera> ();
	}

	// Function is called once per frame
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;
			if (timer <= 0) {
				timer = delay;
				if (main_camera.backgroundColor.Equals (Color.black))
					main_camera.backgroundColor = Color.red;
				else if (main_camera.backgroundColor.Equals (Color.red))
					main_camera.backgroundColor = Color.magenta;
				else
					main_camera.backgroundColor = Color.black;
			}
		}
	}
}
