﻿using UnityEngine;
using System.Collections;
using Types;

public class EarthZombie : Enemy {
	// settings
	private const float walk_speed = 38.0f;
	private const float appear_time = 0.5f; /* from creating object to appearing from ground */
	private const float start_time = 1.0f; /* from appearing from ground to start moving */
	private const float deviation = 60.0f; /* from hero to change direction */
	private const float distance = 140.0f; /* of enemy existence */
	
	// variables
	private bool is_appear = false;
	private float speed = walk_speed;

	// constructor
	private EarthZombie() : base(Spawn.EarthZombie) {
	}

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		my_animation.SwitchAfter ("appear", true, appear_time, NotifyAppear);
		if ((hero.position.x < transform.position.x && direction == true) || (hero.position.x > transform.position.x && direction == false))
			direction = direction ? false : true;
	}

	// Update is called once per frame
	void Update () {
		if (lifes > 0) {
			// select looking direction
			if (transform.position.x <= pos_min && direction == false || transform.position.x >= pos_max && direction == true)
			{
				direction = direction ? false : true;
				NotifyWalk();
			}
			else if (Mathf.Min((int)Mathf.Abs(transform.position.x - pos_min), (int)Mathf.Abs(pos_max - transform.position.x)) > deviation)
			{
				if ((hero.position.x + deviation < transform.position.x && direction == true) || (hero.position.x - deviation > transform.position.x && direction == false))
				{
					direction = direction ? false : true;
					NotifyWalk();
				}
			}
		}
		
		// destroy when out of enemy existence zone
		if (Mathf.Abs (hero.position.x - transform.position.x) > distance)
			NotifyDie ();
	}

	// Weapon hits to the enemy
	public override bool Hit(int damage, int aux_damage) {
		if (is_appear == true && base.Hit(damage, damage) == true) {
			if (lifes <= 0) {
				body.velocity = new Vector2 (0.0f, body.velocity.y);
				my_animation.Switch("walk", false);
			}
			return true;
		} else
			return false;
	}

	// Notifications
	public void NotifyAppear() {
		is_appear = true;
		my_animation.SwitchAfter ("walk", true, start_time, NotifyWalk);
	}
	
	public void NotifyWalk() {
		if (is_appear) {
			speed = direction ? Mathf.Abs (speed) : -Mathf.Abs (speed);
			body.velocity = new Vector2 (speed, 0);
		}
	}
}