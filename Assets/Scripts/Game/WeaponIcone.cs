﻿using UnityEngine;
using System.Collections;

public class WeaponIcone : MonoBehaviour {

	// variables
	public Passport hero = Passport.No;

	// Use this for initialization
	void Start () {
		State.inventory_changed += WeaponChanged;
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Notifications
	void WeaponChanged() {
		if (hero == Passport.No)
			GetComponent<Animator> ().SetInteger ("Weapon", (int)State.Instance.hero.weapon);
		else
			GetComponent<Animator> ().SetInteger ("Weapon", (int)State.Instance.GetHero(Hero.GetName(hero)).weapon);
	}
}