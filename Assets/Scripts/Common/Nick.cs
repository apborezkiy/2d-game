﻿using UnityEngine;
using System.Collections;

public class Nick : MonoBehaviour {
	private TextMesh mesh;

	// variables
	public Passport hero = Passport.No;

	// Use this for initialization
	void Start () {
		State.hero_changed += NickChanged;
		mesh = GetComponent<TextMesh> ();
		NickChanged ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Notifications
	private void NickChanged() {
		string name;
		if (hero == Passport.No)
			name = State.Instance.hero.name;
		else
			name = Hero.GetName (hero);

		mesh.text = name.ToUpper ();
	}
}
