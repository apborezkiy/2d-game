﻿using UnityEngine;
using System.Collections;
using Types;

public class Fox : Enemy {
	// settings
	private const float walk_force_x = 400.0f;
	private const float jump_force_x = 7500.0f;
	private const float jump_force_y = 7000.0f;
	private const float attack_distance = 80.0f;
	private const float prepare_delay = 0.5f;
	private const float max_speed = 100.0f;
	private const float distance = 160.0f; /* of enemy existence */
	
	// variables
	enum Strategy { searching = 0, hunting = 1, prepare = 2, jump = 3, flying = 4, brake = 5, thinking = 6, repetition = 7 };
	Strategy strategy = Strategy.searching;
	
	// constructor
	private Fox() : base(Spawn.Fox) {
	}
	
	// Use this for initialization
	protected override void Start () {
		base.Start ();
		
		// appearance
		Physics2D.IgnoreCollision (hero.GetComponent<Collider2D> (), GetComponent<Collider2D> ());
		transform.position = new Vector3 (hero.transform.position.x + hero.GetComponent<MyCharacterController> ().CastStraight (distance - 20), transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		if (lifes > 0) {
			switch (strategy)
			{
			case Strategy.searching:
				// keep target
				strategy = Strategy.hunting;
				direction = hero.transform.position.x > transform.position.x;
				body.AddForce(new Vector2(direction ? walk_force_x : -walk_force_x, 0.0f));
				GetComponent<Animator>().SetBool("is_walk", true);
				// select looking direction
				if (hero.position.x > transform.position.x)
					direction = true;
				else
					direction = false;
				break;
			case Strategy.hunting:
				// waiting attack distance
				if (Mathf.Abs(hero.transform.position.x - transform.position.x) < attack_distance)
				{
					// stop
					if (hero.GetComponent<Rigidbody2D>().velocity.x != 0)
						strategy = Strategy.jump;
					else
					{
						strategy = Strategy.prepare;
						Scheduler.Instance.Add(new Schedule(prepare_delay, false, new Notifier(NotifyNext)));
					}
					GetComponent<Animator>().SetBool("is_walk", false);
					GetComponent<Animator>().SetBool("is_jump", true);
				} else if (Mathf.Abs(body.velocity.x) < max_speed) {
					body.AddForce(new Vector2(direction ? walk_force_x : -walk_force_x, 0.0f));
				}
				break;
			case Strategy.jump:
				// jump
				body.AddForce(new Vector2(direction? jump_force_x : -jump_force_x, jump_force_y));
				GetComponent<Animator>().SetBool("is_jump", false);
				strategy = Strategy.flying;
				break;
			case Strategy.brake:
				// waiting stop
				if (body.velocity.x == 0.0f)
				{
					strategy = Strategy.thinking;
					Scheduler.Instance.Add(new Schedule(prepare_delay, false, new Notifier(NotifyNext)));
					GetComponent<Animator>().SetBool("is_walk", false);
				}
				break;
			case Strategy.repetition:
				strategy = Strategy.searching;
				break;
			default:
				break;
			}
		}
		
		// destroy when out of enemy existence zone
		if (Mathf.Abs (hero.position.x - transform.position.x) > distance)
			base.NotifyDie ();
	}
	
	public void NotifyNext(object arg) {
		strategy = (Strategy)((int)strategy + 1);
	}

	// Weapon hits to the enemy
	public override bool Hit(int damage, int aux_damage) {
		if (base.Hit(damage, damage) == true) {
			if (lifes <= 0) {
				body.velocity = new Vector2 (0.0f, 0.0f);
				body.isKinematic = true;
				my_animation.Switch("is_walk", false);
			}
			return true;
		} else
			return false;
	}

	// Hero or ground collision detection
	protected override void OnTriggerEnter2D(Collider2D collider) {
		base.OnTriggerEnter2D (collider);
		if (strategy == Strategy.flying && collider.tag == "Ground") {
			GetComponent<Animator>().SetBool("is_walk", true);
			strategy = Strategy.brake;
		}
	}
}