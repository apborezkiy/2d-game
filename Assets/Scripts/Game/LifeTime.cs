﻿using UnityEngine;
using System.Collections;

public class LifeTime : MonoBehaviour {
	// settings
	public float time;

	// variables
	private float timer;

	// Use this for initialization
	void Start () {
		timer = time;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;
			if (timer <= 0)
				Destroy (gameObject);
		}
	}
}