﻿using UnityEngine;
using System.Collections;
using Types;

public enum TeleportType {
	SmoothLeft, SmoothRight, PassUp, PassDown, PassUpLake
};

public class Teleport : MonoBehaviour {

	public Transform target; /* target teleport */
	public Location location; /* target location */
	public Location src_location; /* source location */
	public TeleportType type; /* current teleport kind */
	public bool is_sweam = false;
	public bool is_twin = false;
	public bool is_woods_up = false;
	public float x_map;
	public float y_map;
	private const float offset_down = 16.0f + 5.0f;
	private const float offset_up = 5.0f;
	private const float offset_water = 8.0f;

	// variables
	private bool is_collision = false;
	private GameObject hero;
	private GameObject cam;

	// Use this for initialization
	void Start () {
		hero = GameObject.Find ("Hero");
		cam = GameObject.Find ("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
		if (is_collision == true) {
			if ((Input.GetKeyDown (KeyCode.DownArrow) && type == TeleportType.PassDown) || (Input.GetKeyDown (KeyCode.UpArrow) && type == TeleportType.PassUp)) {
				GetIn ();
			} else if (Input.GetKeyDown (KeyCode.UpArrow) && type == TeleportType.PassUpLake) {
				if (is_sweam == true)
					GetSweamIn ();
				else
					GetSweamOut ();
			}
		}
	}

	// Teleportate any object collisions
	void OnTriggerEnter2D(Collider2D obj) {
		Rigidbody2D body = obj.GetComponent<Rigidbody2D> ();
		if (body != null) {
			if (type == TeleportType.SmoothLeft || type == TeleportType.SmoothRight) {
				if ((body.velocity.x < 0 && type == TeleportType.SmoothRight) || (body.velocity.x > 0 && type == TeleportType.SmoothLeft)) {
					float delta_obj = obj.transform.position.x - transform.position.x;
					float delta_cam = cam.transform.position.x - obj.transform.position.x;
					obj.transform.position = new Vector3 (target.transform.position.x + delta_obj, obj.transform.position.y, obj.transform.position.z);
					// normalize camera if hero
					if (obj.name == "Hero")
						cam.GetComponent<MyCameraFollow> ().LookAt (obj.transform.position.x + delta_cam + delta_obj);
				}
			} else if (obj.name == "Hero") {
				// only hero can use passes
				Navigate();
				is_collision = true;
			} else if (obj.name == "Jason" && location != Location.Unidefined && location != Location.Cave) {
				int cabin = 0;
				if (location == Location.SmallHouse || location == Location.BigHouse || location == Location.WoodsHouse || location == Location.CaveHouse)
					cabin = int.Parse(name.Remove(0, "Cabin".Length));

				if (obj.GetComponent<JIntellect>().Fork(location, cabin)) {
					// teleportate
					float offset_y = is_woods_up? 4.0f : 0.0f;
					if (type == TeleportType.PassUp)
						offset_y += is_twin? offset_up : offset_down;
					else if (type == TeleportType.PassDown)
						offset_y += is_twin? offset_down : offset_up;
					obj.transform.position = new Vector3 (target.transform.position.x, target.transform.position.y + offset_y + 8.0f, obj.transform.position.z);
				}
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D obj) {
		if (obj.name == "Hero")
			is_collision = false;
	}

	// Get in to pass
	private void GetIn() {
		MyCharacterController controller = hero.GetComponent<MyCharacterController> ();
		if (controller.m_is_fly == false && target != null) {
			controller.m_is_sweam = false;
			State.Instance.hero.location = location;
			// stop the hero
			cam.GetComponent<MyCameraFollow>().DisableDuring();
			controller.Stop();
			controller.Standup();
			// move to position
			float offset_y = is_woods_up? 4.0f : 0.0f;
			if (type == TeleportType.PassUp)
				offset_y += is_twin? offset_up : offset_down;
			else if (type == TeleportType.PassDown)
				offset_y += is_twin? offset_down : offset_up;
			float delta_cam = cam.transform.position.x - hero.transform.position.x;
			hero.transform.position = new Vector3 (target.transform.position.x, target.transform.position.y + offset_y, hero.transform.position.z);
			if (location == Location.SmallHouse || location == Location.BigHouse || location == Location.WoodsHouse || location == Location.CaveHouse) {
				State.Instance.hero.house = int.Parse(name.Remove(0, "Cabin".Length));
				Input.ResetInputAxes();
				hero.GetComponent<MyCharacterController>().GetIn();
			} else {
				cam.GetComponent<MyCameraFollow> ().LookAt (hero.transform.position.x + delta_cam);
			}
		}
	}

	// Get in to water
	private void GetSweamIn() {
		MyCharacterController controller = hero.GetComponent<MyCharacterController> ();
		if (controller.m_is_fly == false) {
			is_collision = false;
			cam.GetComponent<MyCameraFollow>().DisableDuring();
			controller.Stop();
			controller.m_is_sweam = true;
			State.Instance.hero.location = location;
			// move to position
			float delta_cam = cam.transform.position.x - hero.transform.position.x;
			hero.transform.position = new Vector3 (target.transform.position.x, target.transform.position.y + offset_water, hero.transform.position.z);
			cam.GetComponent<MyCameraFollow> ().LookAt (hero.transform.position.x + delta_cam);
		}
	}

	// Get out from water
	private void GetSweamOut() {
		MyCharacterController controller = hero.GetComponent<MyCharacterController> ();
		controller.m_is_sweam = false;
		is_collision = false;
		cam.GetComponent<MyCameraFollow>().DisableDuring();
		State.Instance.hero.location = location;
		// move to position
		float delta_cam = cam.transform.position.x - hero.transform.position.x;
		hero.transform.position = new Vector3 (target.transform.position.x, target.transform.position.y + offset_up, hero.transform.position.z);
		cam.GetComponent<MyCameraFollow> ().LookAt (hero.transform.position.x + delta_cam);
	}

	// Update position on the map
	public void Navigate() {
		GameObject o = GameObject.Find ("Face Picker");
		o.transform.localPosition = new Vector3 (x_map, y_map, o.transform.localPosition.z);
	}
}