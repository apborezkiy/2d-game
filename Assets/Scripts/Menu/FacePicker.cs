﻿using UnityEngine;
using System.Collections;

public class FacePicker : MonoBehaviour {
	// synchronization event
	public delegate void FaceTwinkleEvent (bool is_visible);
	public static event FaceTwinkleEvent twinkle_changed;
	
	// settings
	public const float twinkle = 0.25f;
	public int step = 0;
	public bool is_navigator = false;

	// variables
	uint timer_id = 0;
	float z;
	float x;

	// input state
	bool _is_enabled = false;
	public bool is_enabled {
		get { return _is_enabled; }
		set {
			if (_is_enabled == value)
				return;

			_is_enabled = value;
			if (value == false) {
				gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, CameraControl.max_z);
				Scheduler.Instance.Del(ref timer_id);
			} else {
				gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, z);
				timer_id = Scheduler.Instance.Add(new Schedule(twinkle, true, new Types.Notifier(Twinkle)));
			}
		}
	}

	// face icone
	Passport _person = 0;
	Passport person {
		get { return _person; }
		set {
			bool direction = true;
			if (value < _person)
				direction = false;
			Passport alternative = value;
			do
			{
				_person = alternative;
				if (_person < Passport.George)
					_person = Passport.Crissy;
				else if (_person > Passport.Crissy)
					_person = Passport.George;
				alternative = (Passport) (direction? (int) _person + 1 : (int) _person - 1);
			} while (State.Instance.GetHero(Hero.GetName(_person)).health <= 0);
			State.Instance.hero = State.Instance.GetHero(Hero.GetName(_person));
		}
	}

	// Use this for initialization
	void Start () {
		x = gameObject.transform.position.x;
		z = gameObject.transform.position.z;

		if (State.Instance.hero != null)
			person = State.Instance.hero.passport;
	}
	
	// Update is called once per frame
	void Update () {
		if (is_enabled) {
			// default hero
			if (person == Passport.No)
				person = Passport.George;

			// is selector or navigator ?
			if (!is_navigator) {
				// calculate position
				float new_x = x + step * ((int) person - 1);
				gameObject.transform.position = new Vector3(new_x, gameObject.transform.position.y, gameObject.transform.position.z);

				// input
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
					Audio.PlaySong(Audio.Tracks.Pick);
					person++;
				} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					Audio.PlaySong(Audio.Tracks.Pick);
					person--;
				}
			}
		}
	}

	// Notifications
	public void Twinkle(object unsed = null) {
		if (gameObject.transform.position.z != z) {
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
			twinkle_changed(true);
		} else {
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, CameraControl.max_z);
			twinkle_changed(false);
		}
	}

	void OnLevelWasLoaded(int level) {
		twinkle_changed = delegate {};
	}
}