﻿using UnityEngine;
using System.Collections;
using Types;

public class Item : MonoBehaviour {
	// settings
	public Spawn spawn;
	public float time;
	public bool mute;
	private Potions potion = Potions.Empty;
	private PotionEffects effect = PotionEffects.Empty;

	// effects
	private const int sub_health = 5;

	// durations
	private const float time_disable	= 10.0f;
	private const float time_jajadu		= 10.0f;
	private const float time_specular	= 30.0f;
	private const float time_monochrome	= 30.0f;
	private const float time_confuse	= 30.0f;

	// Use this for initialization
	void Start () {
		if (spawn == Spawn.Soda) {
			potion = MakePotion();
			effect = MakeEffect(potion);
			time = potion_times[(int)potion];
			GetComponent<SpriteRenderer> ().material.SetInt ("_Offset", (int)potion);
		}

		if (time != 0.0f)
			Scheduler.Instance.Add (new Schedule (time, false, new Notifier (NotifyDelete, gameObject)));
		if (mute == false)
			Audio.PlaySong (Audio.Tracks.Bonus);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Hero collision check
	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.name == "Hero") {
			switch (spawn) {
			case Spawn.Lighter:
				State.Instance.hero.lighters++;
				Audio.PlaySong(Audio.Tracks.Use);
				break;
			case Spawn.Compote:
				State.Instance.hero.compotes++;
				Audio.PlaySong(Audio.Tracks.Use);
				break;
			case Spawn.Key:
				State.Instance.hero.keys++;
				Audio.PlaySong(Audio.Tracks.Use);
				break;
			case Spawn.Stone:
				State.Instance.hero.weapon = WeaponType.Stone;
				Audio.PlaySong(Audio.Tracks.Use);
				break;
			case Spawn.Knife:
				State.Instance.hero.weapon = WeaponType.Knife;
				Audio.PlaySong(Audio.Tracks.Use);
				break;
			case Spawn.Machete:
				State.Instance.hero.weapon = WeaponType.Machete;
				Audio.PlaySong(Audio.Tracks.Use);
				break;
			case Spawn.Soda:
				ApplyEffect(effect);
				break;
			default:
				break;
			}
			// hero takes item
			Item.NotifyDelete(gameObject);
		}
	}

	// Delete object
	public static void NotifyDelete(object argument) {
		GameObject gameObject = (GameObject)argument;
		if (gameObject != null) {
			Item item = gameObject.GetComponent<Item> ();
			Respawn.Delete (item.spawn);
			GameObject.Destroy (gameObject);
		}
	}

	// Potion location-probability matrix
	private const int potions_count = 7;
	private const int effects_count = 10;
	private static readonly float [,] prob_potions = new float[3, potions_count] {
		/* Cave */				{ 0.3f, 0.2f, 0.4f, 0.5f, 0.5f, 0.5f, 0.5f },
		/* Nothern woods */		{ 0.4f, 0.3f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f },
		/* Southern woods */	{ 0.5f, 0.5f, 0.5f, 0.7f, 0.7f, 0.8f, 0.8f }
	};
	private static readonly float [,] prob_effects = new float[effects_count, potions_count] {
				   /* Pink,  Blue,  Orang, Yello, Red,   Cyan,  Green */
		/* Stone */	{ 0.05f, 0.05f, 0.05f, 0.10f, 0.10f, 0.30f, 0.20f },
		/* Um */	{ 0.10f, 0.40f, 0.15f, 0.10f, 0.10f, 0.05f, 0.05f },
		/* MinH */	{ 0.10f, 0.30f, 0.10f, 0.15f, 0.15f, 0.25f, 0.15f },
		/* MaxH */	{ 0.20f, 0.05f, 0.10f, 0.10f, 0.30f, 0.10f, 0.05f },
		/* Par */	{ 0.15f, 0.05f, 0.05f, 0.40f, 0.10f, 0.05f, 0.15f },
		/* SubH */	{ 0.15f, 0.25f, 0.05f, 0.15f, 0.10f, 0.20f, 0.15f },
		/* Refl */	{ 0.10f, 0.01f, 0.05f, 0.05f, 0.05f, 0.05f, 0.35f },
		/* Mono */	{ 0.05f, 0.01f, 0.05f, 0.15f, 0.05f, 0.10f, 0.05f },
		/* Conf */	{ 0.01f, 0.01f, 0.25f, 0.10f, 0.05f, 0.40f, 0.40f },
		/* Jaja */	{ 0.01f, 0.01f, 0.10f, 0.35f, 0.05f, 0.10f, 0.40f }
	};
	private static readonly float [] potion_times = new float[potions_count] {
		/* Pink, Blue, Oran, Yell, Red,  Cyan, Gree */
		   4.0f, 3.0f, 3.0f, 5.0f, 5.0f, 6.0f, 6.0f
	};

	// Generate suitable potion effect
	public static PotionEffects MakeEffect(Potions potion) {
		int j = (int) potion;

		float [] probs = new float[effects_count]; 
		for (int i = 0; i < effects_count; ++i)
			probs [i] = prob_effects [i, j];

		PotionEffects effect = (PotionEffects) NextElement (probs, effects_count);

		return effect;
	}

	// Generate suitable potion at the specified location
	public static Potions MakePotion() {
		int i = -1;
		if (State.Instance.hero.location == Location.Cave)
			i = 0;
		else if (State.Instance.hero.location == Location.NorthernWoods)
			i = 1;
		else if (State.Instance.hero.location == Location.SouthernWoods)
			i = 2;

		float [] probs = new float[potions_count];
		for (int j = 0; j < potions_count; ++j)
			probs [j] = prob_potions [i, j];

		Potions potion = (Potions) NextElement (probs, potions_count);

		return potion;
	}

	// Get the random potion by the probability matrix
	public static int NextElement(float [] probs, int n) {
		float max_prob = 0.0f;
		for (int i = 0; i < n; ++i) {
			if (max_prob < probs[i])
				max_prob = probs[i];
		}
		
		int num = Cabins.Instance.random.Next () % (int)(max_prob * 100);
		
		int felix_count = 0;
		int [] felixes = new int[n];
		
		for (int i = 0; i < n; ++i) {
			if ((float) num <= probs[i] * 100.0f)
				felixes[felix_count++] = i;
		}
		
		int felix = felixes[Cabins.Instance.random.Next() % felix_count];
		return felix;
	}

	// Effects
	public static void ApplyEffect(PotionEffects effect) {
		System.Func<bool, float> fun = null;
		switch (effect) {
		case PotionEffects.Stone: /* weapon */
			Audio.PlaySong(Audio.Tracks.Failure);
			State.Instance.hero.weapon = WeaponType.Stone;
			break;
		case PotionEffects.Um: /* null health */
            Audio.PlaySong(Audio.Tracks.Die);
			GameObject.Find("Hero").GetComponent<MyCharacterController>().Hit(State.Instance.hero.health, true);
			break;
		case PotionEffects.MinHealth: /* min health */
			Audio.PlaySong(Audio.Tracks.Failure);
			GameObject.Find("Hero").GetComponent<MyCharacterController>().Hit(State.Instance.hero.health - 1, true);
			break;
		case PotionEffects.MaxHealth: /* max health */
			Audio.PlaySong(Audio.Tracks.AddHealth);
			State.Instance.hero.health = Hero.max_health;
			break;
		case PotionEffects.Disable: /* disable for a time */
			Audio.PlaySong(Audio.Tracks.Disable);
			GameObject.Find("Hero").GetComponent<MyCharacterController>().DisableEffect(time_disable);
			break;
		case PotionEffects.SubHealth: /* sub health */
			Audio.PlaySong(Audio.Tracks.SubHealth);
			GameObject.Find("Hero").GetComponent<MyCharacterController>().Hit(sub_health, true);
			break;
		case PotionEffects.Specular: /* reflection */
			Audio.PlaySong(Audio.Tracks.Confuse);
			fun = delegate(bool value) { Effector.is_reflected = value; return time_specular; };
			EnableEffect(fun, Audio.Tracks.AntiConfuse);
			break;
		case PotionEffects.Monochrome: /* monochrome */
			Audio.PlaySong(Audio.Tracks.Confuse);
			fun = delegate(bool value) { Effector.is_monochrome = value; return time_monochrome; };
            EnableEffect(fun, Audio.Tracks.AntiConfuse);
			break;
		case PotionEffects.Confuse: /* intensity */
			Audio.PlaySong(Audio.Tracks.Confuse);
			fun = delegate(bool value) { Effector.is_fintensity = value; return time_confuse; };
            EnableEffect(fun, Audio.Tracks.AntiConfuse);
			break;
		case PotionEffects.Jajadu:
			Audio.PlaySong(Audio.Tracks.Confuse);
			GameObject.Find("Hero").GetComponent<MyCharacterController>().JajaduEffect(time_jajadu);
			break;
		default:
			break;
		}
	}

	// Effects control
	public static void EnableEffect(System.Func<bool, float> fun, Audio.Tracks track) {
		float time = fun (true);
		Scheduler.Instance.Add (new Schedule (time, false, new Notifier (NotifyDisabled, fun)));
        Scheduler.Instance.Add(new Schedule(time, false, new Notifier(NotifyPlay, track)));
	}

	public static void NotifyDisabled(object fun) {
		((System.Func<bool, float>)fun) (false);
	}

    public static void NotifyPlay(object track) {
        Audio.PlaySong((Audio.Tracks)track);
    }

	// Postprocessing
	public static ScreenEffect Effector { get { return GameObject.Find("Main Camera").GetComponent<ScreenEffect>(); } }
}
