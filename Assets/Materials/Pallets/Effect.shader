﻿// Upgrade NOTE: replaced 'samplerRECT' with 'sampler2D'
// Upgrade NOTE: replaced 'texRECT' with 'tex2D'

// Upgrade NOTE: replaced 'samplerRECT' with 'sampler2D'
// Upgrade NOTE: replaced 'texRECT' with 'tex2D'

Shader "Custom/Effect"
{  
     Properties
     {
     	_MainTex ("Main Texture", 2D) = "white" {}
     }
     SubShader
     {
         Tags
         {
         	 "Queue"="Overlay" 
             "IgnoreProjector"="True" 
             "RenderType"="Transparent" 
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
         LOD 100
         Cull Off
         ZWrite On
         Blend Off
 
         Pass
         {
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #include "UnityCG.cginc"
             
             // variables
             float4 _MainTex_ST;
             sampler2D _MainTex;
             sampler2D _Pallet;
             int _Offset;
             int _Size;
             float _step_x;
             float _step_y;
             
             // fragment settings
             int _is_blur;
             int _is_fintensity;
             int _is_monochrome;
             int _is_infared;
             
             // vertex settings
             int _is_reflected;
             
             //
             // auxiliary vertex funtion
             //
             float4 v_reflect(float4 v)
             {
                 v.x = -v.x;
                 v.y = -v.y;
                 
                 return v;
             }
             
             //
             // auxiliary fragment functions
             //
             float4 f_blur(float2 uv_MainTex)
             {
                 float4 c = tex2D(_MainTex, uv_MainTex);
                 float4 c1 = tex2D(_MainTex, float2(uv_MainTex.x + _step_x, uv_MainTex.y + _step_y));
                 float4 c2 = tex2D(_MainTex, float2(uv_MainTex.x + _step_x, uv_MainTex.y - _step_y));
                 float4 c3 = tex2D(_MainTex, float2(uv_MainTex.x - _step_x, uv_MainTex.y + _step_y));
                 float4 c4 = tex2D(_MainTex, float2(uv_MainTex.x - _step_x, uv_MainTex.y - _step_y));
                 
                 return (c + c1 + c2 + c3 + c4) / 5;
             }
             
             float4 f_fintensity(float4 c)
             {
                 const float intensity = 1.5;
                 return c * intensity * cos(_Time);
             }
             
             float4 f_monochrome(float4 c)
             {
                 float ch = (c.r + c.g + c.b) / 3;
                 return float4(ch, ch, ch, c.a);
             }
             
             float4 f_infared(float4 c)
             {
                 return float4(c.r, 0, 0, c.a);
             }
             
             //
             // main functions
             //
             struct VS_INPUT
             {
                 float4 vert : POSITION;
                 float2 uv_MainTex : TEXCOORD0;
             };
     
             struct FS_INPUT
             {
                 float4 vert : SV_POSITION;
                 float2 uv_MainTex : TEXCOORD0;
             };

             FS_INPUT vert(VS_INPUT i)
             {
                 FS_INPUT o;
     
                 o.vert = mul(UNITY_MATRIX_MVP, i.vert);
                 
                 if (_is_reflected != 0)
                 	o.vert = v_reflect(o.vert);
                 
                 o.uv_MainTex = TRANSFORM_TEX(i.uv_MainTex, _MainTex);
     
                 return o;
             }
                                                     
             float4 frag(FS_INPUT i) : COLOR
             {
                 float4 c = tex2D(_MainTex, i.uv_MainTex);
                 
                 if (_is_blur != 0)
                 	c = f_blur(i.uv_MainTex);
                 	
                 if (_is_fintensity != 0)
                 	c = f_fintensity(c);
                 	
                 if (_is_infared != 0)
                 	c = f_infared(c);
                 	
                 if (_is_monochrome != 0)
                 	c = f_monochrome(c);
                 
                 return c;
             }
 
             ENDCG
         }
     }
}