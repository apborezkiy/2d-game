﻿using UnityEngine;
using System;
using System.Collections;
using Types;

public class State : MonoBehaviour {
	public delegate void HeroEvent();
	public delegate void StatusEvent(Passport passport);
	public static event HeroEvent hero_changed;
	public static event HeroEvent kids_changed;
	public static event HeroEvent health_changed;
	public static event HeroEvent inventory_changed;
	public static event StatusEvent aux_status_changed;
	public static event HeroEvent detector_changed;

	// settings
	public const int max_house = 10;
	public const int max_hero = 6;

	// heroes and bosses
	public ArrayList heroes = new ArrayList ();
	public ArrayList bosses = new ArrayList ();

	// active hero
	public Hero _hero = null;
	public Hero hero {
		get { return _hero; }
		set {
			_hero = value;
			hero_changed ();
		}
	}

	// current number of childrens
	public const int kids_house = 20;
	private int _kids = 15;
	public int kids {
		get { return _kids; }
		set {
			_kids = value;
			kids_changed ();
		}
	}

	// bosses information
	public bool is_battle = false;
	private bool _is_hero_detector = false;
	private bool _is_kids_detector = false;

	public bool is_hero_detector {
		get { return _is_hero_detector; }
		set {
			_is_hero_detector = value;
			detector_changed ();
		}
	}
	public bool is_kids_detector {
		get { return _is_kids_detector; }
		set {
			_is_kids_detector = value;
			detector_changed ();
			if (kids <= 0)
				Scheduler.Instance.Add(new Schedule(2.0f, false, new Notifier((object unused) => Application.LoadLevel("Menu"))));
		}
	}

	// current location and the time of day
	private Weather _weather = Weather.Morning;
	public Weather weather {
		get { return _weather; }
		set { _weather = value;
			GameObject.Find("House Sprite").GetComponent<SpriteRenderer>().sharedMaterial.SetInt("_Offset", (int)_weather);
			GameObject.Find("Around Lake Sprite").GetComponent<SpriteRenderer>().sharedMaterial.SetInt("_Offset", (int)_weather);
			GameObject.Find("Around Cave Sprite").GetComponent<SpriteRenderer>().sharedMaterial.SetInt("_Offset", (int)_weather);
			MeshRenderer [] list = (MeshRenderer [])GameObject.FindObjectsOfType(typeof(MeshRenderer));
			foreach (MeshRenderer o in list) {
				if (o.sharedMaterial.shader.name == "Custom/Pallete") {
					o.sharedMaterial.SetInt("_Offset", (int)_weather);
				}
			}
		}
	}

	// interlevel settings
	public bool is_trip = true;
	public bool is_title = true;
	public bool is_jump = false;
	public bool is_the_end = false;

	// randomizer
	System.Random random = new System.Random();

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad (this);
		// init heroes
		heroes.Add(new Hero(Passport.George, GenerateHouse(), 0, false, 1.0f, 1.0f, 1.0f, 1.0f));
		heroes.Add(new Hero(Passport.Mark, GenerateHouse(), 1, false, 1.0f, 1.0f, 1.0f, 1.0f));
		heroes.Add(new Hero(Passport.Paul, GenerateHouse(), 2, false, 1.0f, 1.0f, 1.0f, 1.0f));
		heroes.Add(new Hero(Passport.Laura, GenerateHouse(), 3, true, 1.0f, 1.0f, 1.0f, 1.0f));
		heroes.Add(new Hero(Passport.Debbie, GenerateHouse(), 4, true, 1.0f, 1.0f, 1.0f, 1.0f));
		heroes.Add(new Hero(Passport.Crissy, GenerateHouse(), 5, true, 1.0f, 1.0f, 1.0f, 1.0f));
		_hero = GetHero (Hero.GetName(Passport.George));
		// init bosses
		bosses.Add (new Boss (Passport.Jason, 32 * 10, 0, 0, 0, Location.AroundCave, 120.0f, 40.0f));
		bosses.Add (new Boss (Passport.Head, 8 * 10, 0, 2, 0, Location.CaveHouse, 10.0f, 10.0f));
	}

	// Use this for initialization
	void Start() {
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Selected hero
	public Hero GetHero(String name) {
		foreach (Hero element in heroes) {
			if (element.name == name)
				return element;
		}
		return null;
	}

	// Get boss by passport
	public Boss GetBoss(Passport passport) {
		foreach (Boss element in bosses) {
			if (element.passport == passport)
				return element;
		}
		return null;
	}

	// Get boss by house
	public Boss GetBossByHouse(int house) {
		foreach (Boss element in bosses) {
			if (element.house == house)
				return element;
		}
		return null;
	}

	// Get hero in house
	public Passport GetHeroByHouse(int house) {
		foreach (Hero element in heroes) {
			if (element.house == house)
				return element.passport;
		}
		return Passport.No;
	}

	// Information
	public bool IsAnyHeroAlive() {
		foreach (Hero element in heroes) {
			if (element.health > 0)
				return true;
		}
		return false;
	}

	public bool IsSeveralHeroes() {
		bool found = false;
		foreach (Hero element in heroes) {
			if (element.health > 0) {
				if (!found)
					found = true;
				else
					return true;
			}
		}
		return false;
	}

	public uint GetStatistics(Spawn type, uint count) {
		switch (type) {
		case Spawn.Lighter:
			return hero.lighters;
		default:
			return count;
		}
	}

	// Generate random house
	private int GenerateHouse(int offset = 0) {
		int house = offset > max_house? 1 : offset;
		if (house == 0)
			house = 1 + random.Next () % max_house;
		foreach (Hero element in heroes) {
			if (element.house == house)
				return GenerateHouse(house + 1);
		}
		return house;
	}
	
	// Game state instance
	private static State instance = null;
	public static State Instance {
		get {
			State state = null;
			GameObject obj = GameObject.Find ("State");
			if (obj != null) {
				state = obj.GetComponent<State>();
				return state;
			} else if (instance != null) {
				return instance;
			} else {
				GameObject original = GameObject.Find ("State Reference");
				instance = original.AddComponent<State>();
				return instance;
			}
		}
	}

	// Send events
	public static void HealthChanged() {
		health_changed ();
	}

	public static void InventoryChanged() {
		inventory_changed ();
	}

	public static void AuxStatusChanged(Passport passport) {
		aux_status_changed (passport);
		hero_changed ();
		inventory_changed ();
	}

	void OnLevelWasLoaded(int level) {
		hero_changed = delegate {};
		kids_changed = delegate {};
		health_changed = delegate {};
		inventory_changed = delegate {};
		aux_status_changed = delegate {};
		detector_changed = delegate {};
	}
}