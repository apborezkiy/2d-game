﻿using UnityEngine;
using System.Collections;

public class FaceIco : MonoBehaviour {

	// variables
	public Passport hero = Passport.No;
	private float z;
	
	// Use this for initialization
	void Start () {
		State.hero_changed += FaceChanged;
		z = gameObject.transform.position.z;
		FaceChanged ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	// Notifications
	private void FaceChanged() {
		int health;
		if (hero == Passport.No)
			health = State.Instance.hero.health;
		else
			health = State.Instance.GetHero (Hero.GetName (hero)).health;

		if (health <= 0 && gameObject.transform.position.z == z)
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, CameraControl.max_z);
		else if (health > 0 && gameObject.transform.position.z != z)
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, z);
	}
}
