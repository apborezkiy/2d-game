﻿using System;

namespace Types {
	// types definitions
	public enum Location {
		Unidefined,
		SmallHouse, BigHouse,
		Lake, AroundLake,
		Cave, AroundCave,
		NorthernWoods, SouthernWoods, AroundWoods,
		WoodsHouse, CaveHouse
	};

	public enum Weather {
		Morning = 0,
		Evening = 1,
		Night = 2
	};

	public enum Potions {
		Pink = 0, Blue = 1, Orange = 2, Yellow = 3, Red = 4, Cyan = 5, Green = 6,
		Empty = 99
	};

	public enum PotionEffects {
		Mouse, MaxHealth = 3, AntiJason, GoodJasonMood, GoodLuck, AndiJason,
		Gop, Post, Um = 1, MinHealth = 2, SubHealth = 5, Jason, Rage, Stone = 0, BadJasonMood, BadLuck,
		Confuse = 8, Jajadu = 9, Specular = 6, Disable = 4, SlowDown, Monochrome = 7,
		Empty = 99
	};

	public enum Spawn {
		EarthZombie = 0, AquaZombie = 7, Raven = 8, Fox = 9, Wolf = 10,
		Stone = 4, Knife = 5, Machete = 6,
		Lighter = 1, Compote = 2, Key = 3,
		Letter = 11, MacheteInHouse = 12, AxeInHouse = 13, FireInHouse = 14,
		Boy = 15, Girl = 16, Kid = 17,
		FireInFireplace = 18, Grate = 19, Lantern = 20, Door = 21, Soda = 22,
		/* phony */
		Jason = 97, Head = 98,
		/* do not change this */
		Empty = 99
	};

	public enum DoorState { Absent, Locked, Opened };

	public class Notifier {
		// types definition
		public delegate void Notify(Object arg);

		// variables
		private Notify notify;
		private Object arg;

		// constructor
		public Notifier(Notify notify, Object arg = null) {
			this.notify = notify;
			this.arg = arg;
		}

		// Start notify
		public void Start() {
			this.notify (this.arg);
		}
	}
}