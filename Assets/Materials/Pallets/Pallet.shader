﻿// Upgrade NOTE: replaced 'samplerRECT' with 'sampler2D'
// Upgrade NOTE: replaced 'texRECT' with 'tex2D'

// Upgrade NOTE: replaced 'samplerRECT' with 'sampler2D'
// Upgrade NOTE: replaced 'texRECT' with 'tex2D'

Shader "Custom/Pallete"
{  
     Properties
     {
     	_MainTex ("Sprite Texture", 2D) = "white" {}
        _Pallet ("Pallet Texture", 2D) = "white" {}
        _Offset ("Current offset", Int) = 0
        _Size ("Pallet size", Int) = 0
     }
     SubShader
     {
         Tags
         {
         	 "Queue"="Transparent" 
             "IgnoreProjector"="True" 
             "RenderType"="Transparent" 
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
         LOD 100
         Cull Off
         ZWrite Off
         Blend SrcAlpha OneMinusSrcAlpha
 
         Pass
         {
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma multi_compile DUMMY PIXELSNAP_ON
             #include "UnityCG.cginc"
             
             float4 _MainTex_ST;
             sampler2D _MainTex;
             sampler2D _Pallet;
             int _Offset;
             int _Size;
 
             struct VS_INPUT
             {
                 float4 vert : POSITION;
                 float2 uv_MainTex : TEXCOORD0;
             };
     
             struct FS_INPUT
             {
                 float4 vert : SV_POSITION;
                 float2 uv_MainTex : TEXCOORD0;
             };

             FS_INPUT vert(VS_INPUT i)
             {
                 FS_INPUT o;
     
                 o.vert = mul(UNITY_MATRIX_MVP, i.vert);
                 o.uv_MainTex = TRANSFORM_TEX(i.uv_MainTex, _MainTex);
     
                 return o;
             }
                                                     
             float4 frag(FS_INPUT i) : COLOR
             {
                 float x = tex2D(_MainTex, i.uv_MainTex).r;
                 float y = (1.0 / (_Size - 1)) * _Offset;
                 float4 c = tex2D(_Pallet, float2(x, y)).rgba;
                 
                 return c;
             }
 
             ENDCG
         }
     }
}