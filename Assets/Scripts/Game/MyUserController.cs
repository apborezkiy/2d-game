﻿using UnityEngine;
using System.Collections;

public class MyUserController : MonoBehaviour {
	private MyCharacterController character;

	// settings
	public bool is_active = true;

	// Use this for initialization
	void Start () {
		character = GetComponent<MyCharacterController>();
	}
	
	// Function is called once per frame
	void Update () {
		if (is_active == false)
			return;

		// debug
		if (Input.GetKeyDown (KeyCode.F12))
			Audio.Instance.is_user = !Audio.Instance.is_user;

		// user controller
		if (character.m_is_cabin == false) {
			// outdoor control scheme
			if (Input.GetKeyDown("return")) {
				character.SwitchCamera();
			}

			if (Input.GetKeyDown (KeyCode.DownArrow))
				character.Sitdown ();
			else if (Input.GetKeyUp (KeyCode.DownArrow))
				character.Standup ();

			if (Input.GetKeyDown (KeyCode.LeftControl))
				character.Shoot ();
			else if (Input.GetKeyDown (KeyCode.LeftAlt))
				character.Jump ();
			else if (Input.GetKey (KeyCode.RightArrow)) {
				if (character.m_is_sweam == false)
					character.Walk (!character.m_is_jajadu ? true : false);
				else
					character.Sweam (!character.m_is_jajadu ? true : false);
			} else if (Input.GetKey (KeyCode.LeftArrow)) {
				if (character.m_is_sweam == false)
					character.Walk (!character.m_is_jajadu ? false : true);
				else
					character.Sweam (!character.m_is_jajadu ? false : true);
			} else {
				if (character.m_is_sweam == false)
					character.Stop ();
				else
					character.Hamper ();
			}

			if (Input.GetKeyDown (KeyCode.Q))
				Lantern.Turn();

		} else if (character.m_is_cabin == true) {
			if (character.m_is_battle == false) {
				if (Input.GetKeyDown("return")) {
					// load map
					State.Instance.is_trip = false;
					State.Instance.is_title = false;
					State.Instance.is_jump = true;
					Application.LoadLevel ("Menu");
				} else {
					if (!State.Instance.hero.is_action_selector) {
						// indoor wandering control
						if (Input.GetKeyDown (KeyCode.UpArrow))
							character.GoStraight();
						else if (Input.GetKeyUp (KeyCode.UpArrow))
							character.StopStraight();
						else if (character.m_is_enabled == true)
						{
							if (Input.GetKeyDown (KeyCode.DownArrow))
								character.Rotate (180);
							else if (Input.GetKeyDown (KeyCode.RightArrow))
								character.Rotate (!character.m_is_jajadu ? 90 : 270);
							else if (Input.GetKeyDown (KeyCode.LeftArrow))
								character.Rotate (!character.m_is_jajadu ? 270 : 90);
							if (Input.GetKeyDown (KeyCode.Tab)) {
								State.Instance.hero.is_action_selector = true;
								Action.UpdateSelector(State.Instance.hero.is_action_selector, State.Instance.hero.action_selector);
							}
						} else {
							if (Input.GetKeyDown (KeyCode.LeftControl))
								Action.Continue();
						}
					} else {
						if (character.m_is_enabled == true) {
							// indoor inventory control
							if (Input.GetKeyDown (KeyCode.DownArrow)) {
								State.Instance.hero.action_selector++;
								Action.UpdateSelector(true, State.Instance.hero.action_selector);
							} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
								State.Instance.hero.action_selector--;
								Action.UpdateSelector(true, State.Instance.hero.action_selector);
							} else if (Input.GetKeyDown (KeyCode.LeftAlt)) {
								State.Instance.hero.is_action_selector = false;
								Action.Perform(State.Instance.hero.action_selector);
								State.Instance.hero.action_selector = 0;
								Action.UpdateSelector(State.Instance.hero.is_action_selector, State.Instance.hero.action_selector);
							} else if (Input.GetKeyDown (KeyCode.LeftControl)) {
								State.Instance.hero.is_action_selector = false;
								State.Instance.hero.action_selector = 0;
								Action.UpdateSelector(State.Instance.hero.is_action_selector, State.Instance.hero.action_selector);
							}
						}
					}
				}
			} else {
				if (character.m_is_enabled == true) {
					// indoor battle control
					if (Input.GetKeyDown (KeyCode.LeftControl))
						character.ShootStraight ();
					else if (Input.GetKey (KeyCode.DownArrow) && Input.GetKeyDown (KeyCode.RightArrow))
						character.Bend(!character.m_is_jajadu ? true : false);
					else if (Input.GetKey (KeyCode.DownArrow) && Input.GetKeyDown (KeyCode.LeftArrow))
						character.Bend(!character.m_is_jajadu ? false : true);
					else if (Input.GetKey (KeyCode.RightArrow))
						character.WalkIn (!character.m_is_jajadu ? true : false);
					else if (Input.GetKey (KeyCode.LeftArrow))
						character.WalkIn (!character.m_is_jajadu ? false : true);
					else
						character.StopStraight();
				}
			}
		}
	}
}