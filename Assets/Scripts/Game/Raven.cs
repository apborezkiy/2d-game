﻿using UnityEngine;
using System.Collections;
using Types;

public class Raven : Enemy {
	// settings
	private const float fly_speed = 35.0f;
	private const float attack_speed_x = 35.0f;
	private const float attack_speed_y = 185.0f;
	private const float flight_speed_x = 120.0f;
	private const float flight_speed_y = 40.0f;
	private const float attack_distance = 45.0f;
	private const float distance = 160.0f; /* of enemy existence */
	
	// variables
	private bool is_attacked = false;
	private bool is_flight = false;
	
	// constructor
	private Raven() : base(Spawn.Raven) {
	}
	
	// Use this for initialization
	protected override void Start () {
		base.Start ();
		body.isKinematic = true;

		// appearance
		transform.position = new Vector3 (hero.transform.position.x + hero.GetComponent<MyCharacterController> ().CastStraight (distance - 20), transform.position.y, transform.position.z);

		// select looking direction
		if (hero.position.x > transform.position.x)
			direction = true;

		// start flying
		body.velocity = new Vector2 (direction? fly_speed : -fly_speed, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (lifes > 0) {
			if (!is_attacked && Mathf.Abs(hero.transform.position.x - transform.position.x) < attack_distance)
			{
				// attack the hero
				GetComponent<Animator>().SetBool("is_attack", true);
				body.velocity = new Vector2 (direction? attack_speed_x : -attack_speed_x, -attack_speed_y);
				is_attacked = true;
			} else if (!is_flight && transform.position.y < hero.transform.position.y) {
				GetComponent<Animator>().SetBool("is_attack", false);
				body.velocity = new Vector2 (direction? flight_speed_x : -flight_speed_x, flight_speed_y);
				is_flight = true;
			}
		}
		
		// destroy when out of enemy existence zone
		if (Mathf.Abs (hero.position.x - transform.position.x) > distance)
			base.NotifyDie ();
	}

	// Weapon hits to the enemy
	public override bool Hit(int damage, int aux_damage) {
		if (base.Hit(damage, damage) == true) {
			if (lifes <= 0) {
				body.velocity = new Vector2 (0.0f, 0.0f);
				my_animation.Switch("is_die", true);
			}
			return true;
		} else
			return false;
	}
}