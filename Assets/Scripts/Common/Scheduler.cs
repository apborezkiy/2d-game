﻿using UnityEngine;
using System.Collections.Generic;
using Types;

public class Schedule
{
	public Notifier notifier; /* callback for notification */
	public float timer; /* current timer value */
	public float time; /* start timer value */
	public bool is_loop; /* cyclic timer */
	public uint id; /* identifier of the loop timer */
	
	// constructor
	public Schedule(float time, bool is_loop, Notifier notifier) {
		this.notifier = notifier;
		this.timer = time;
		this.time = time;
		this.is_loop = is_loop;
		this.id = 0;
	}
};

public class Scheduler : MonoBehaviour {
	// variables
	private List<Schedule> schedules;
	public float time = 0.0f;

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad (this);
		schedules = new List<Schedule> ();
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		// update timers and find expired schedules
		List<Schedule> expired = new List<Schedule> ();
		foreach (Schedule schedule in schedules) {
			schedule.timer -= Time.deltaTime;
			if (schedule.timer <= 0) {
				if (schedule.is_loop == true)
					schedule.timer = schedule.time;
				else
					schedule.timer = 0.0f;
				expired.Add(schedule);
			}
		}
		// delete disposable expired schedules
		schedules.RemoveAll (schedule => schedule.timer == 0.0f);
		// notify expired schedules
		foreach (Schedule schedule in expired) {
			schedule.notifier.Start();
		}
	}

	// Register new timer
	public uint Add(Schedule schedule) {
		// search maximal free identifier
		uint id = 0;
		if (schedule.is_loop == true) {

			foreach (Schedule _schedule in schedules) {
				if (id < _schedule.id)
					id = _schedule.id;
			}
			schedule.id = ++id;
		}
		// put to the queue
		schedules.Add (schedule);
		return schedule.id;
	}

	// Unregister timer
	public void Del(ref uint id) {
		// delete schedule by identifier
		uint _id = id;
		if (_id != 0) {
			schedules.RemoveAll (schedule => schedule.id == _id);
			id = 0;
		}
	}

	// Instance
	private static Scheduler instance = null;
	public static Scheduler Instance {
		get {
			if (instance == null) {
				GameObject original = GameObject.Find ("Scheduler Reference");
				instance = original.AddComponent<Scheduler>();
			}

			return instance;
		}
	}

	// Events
	void OnLevelWasLoaded(int level) {
		schedules.Clear ();
	}
}
