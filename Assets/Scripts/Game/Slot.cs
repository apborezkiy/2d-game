﻿using UnityEngine;
using System.Collections;
using Types;

public class Slot : MonoBehaviour {
	public Spawn spawn;
	public bool is_text = false;
	public bool is_visible = true;
	public bool is_aborigine_oriented = true;

	// variables
	private float x_origin;
	private const float x_offset = 256.0f;

	// Use this for initialization
	void Start () {
		if (spawn == Spawn.Empty)
			State.aux_status_changed += AuxStatusChanged;
		else
			State.inventory_changed += SlotChanged;

		x_origin = transform.localPosition.x;

		SlotChanged ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	// Notifications
	private void SlotChanged() {
		uint value = 0;

		switch (spawn) {
		case Spawn.Lighter:
			value = State.Instance.hero.lighters;
			break;
		case Spawn.Compote:
			value = State.Instance.hero.compotes;
			break;
		case Spawn.Key:
			value = State.Instance.hero.keys;
			break;
		case Spawn.Lantern:
			value = State.Instance.hero.lanterns;
			break;
		case Spawn.Empty:
			value = 1; /* phony visible */
			break;
		}

		if (value != 0 && is_visible) {
			if (transform.localPosition.x != x_origin)
				transform.localPosition = new Vector3 (x_origin, transform.localPosition.y, transform.localPosition.z);
		} else {
			if (transform.localPosition.x != x_origin + x_offset)
				transform.localPosition = new Vector3 (x_origin + x_offset, transform.localPosition.y, transform.localPosition.z);
		}

		if (is_text) {
			if (spawn == Spawn.Lantern) {
				if (State.Instance.hero.lanterns < Hero.lantern_capacity / 3)
					GetComponent<TextMesh>().text = "L";
				else if (State.Instance.hero.lanterns < 2 * Hero.lantern_capacity / 3)
					GetComponent<TextMesh>().text = "M";
				else
					GetComponent<TextMesh>().text = "H";
			} else {
				GetComponent<TextMesh>().text = (Mathf.Min(value, 9)).ToString();
			}
		}
	}

	private void AuxStatusChanged(Passport passport) {
		if (is_aborigine_oriented) {
			// for heroes
			is_visible = (passport >= Passport.George) && (passport <= Passport.Crissy);
		} else {
			// for enemies
			is_visible = (passport >= Passport.Jason) && (passport <= Passport.Head);
		}

		// update values
		if (is_visible) {
			LifesBar lbc = GetComponent<LifesBar>();
			WeaponIcone wic = GetComponent<WeaponIcone>();
			Nick nc = GetComponent<Nick>();
			if (lbc != null)
				lbc.hero = passport;
			if (wic != null)
				wic.hero = passport;
			if (nc != null)
				nc.hero = passport;
		}

		SlotChanged ();
	}
}
