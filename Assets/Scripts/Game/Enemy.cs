﻿using UnityEngine;
using System.Collections;
using Types;

public abstract class Enemy : MonoBehaviour {
	// settings
	public int damage;
	public int lifes;
	public float pos_min;
	public float pos_max;
	private const float hit_time = 0.2f;
	protected const float die_time = 0.5f;

	// variables
	protected Spawn spawn;
	protected Transform hero;
	protected MyAnimationController my_animation;
	protected Rigidbody2D body;
	private bool _direction = false;
	protected bool direction {
		get { return _direction; }
		set {
			if (_direction != value)
			{
				// change looking direction
				_direction = value;
				Vector3 scale = transform.localScale;
				scale.x *= -1;
				transform.localScale = scale;
			}
		}
	}

	// constructor
	protected Enemy(Spawn spawn) {
		this.spawn = spawn;
	}

	// Use this for initialization
	protected virtual void Start() {
		my_animation = GetComponent<MyAnimationController> ();
		body = GetComponent<Rigidbody2D> ();
		hero = GameObject.Find ("Hero").GetComponent<Transform>();
	}

	// Weapon hits to the enemy
	public virtual bool Hit(int damage, int aux_damage) {
		if (lifes > 0) {
			Audio.PlaySong(Audio.Tracks.Wound);
			lifes -= damage;
			if (lifes <= 0) {
				State.Instance.hero.kills++;
				my_animation.Twinkle(die_time, NotifyDie);
			} else
				my_animation.Twinkle(hit_time);
			return true;
		}

		return false;
	}

	// Hero collision detection
	protected virtual void OnTriggerEnter2D(Collider2D collider) {
		if (collider.name == "Hero" && lifes > 0) {
			hero.GetComponent<MyCharacterController> ().Hit (damage);
		}
	}

	// Notifications
	public virtual void NotifyDie() {
		Respawn.Delete (spawn);
		Destroy (gameObject);
	}
}